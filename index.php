<?php
    session_start();
    require_once(__DIR__."/libraries/User.php");
    $login = new User();

    if($login->isLoggedIn()!="")
    {
        $userLevel = $login->getUserLevel($_SESSION['user_session']);

        if($userLevel == config::STUDENT_LEVEL)
        {
            $login->redirect('./pages/studentMain.php');
        }
        else if($userLevel == config::TEACHER_LEVEL)
        {
            $login->redirect('./pages/teacherMain.php');
        }
        else if($userLevel == config::ADMIN_LEVEL)
        {
            $login->redirect('./pages/adminMain.php');
        }
    }

    if(isset($_POST['submit']))
    {
        $uname = strip_tags($_POST['loginName']);
        $upass = strip_tags($_POST['pwd']);

        if($login->doLogin($uname, $upass))
        {
            $userLevel = $login->getUserLevel($_SESSION['user_session']);

            if($userLevel == config::STUDENT_LEVEL)
            {
                $login->redirect('./pages/studentMain.php');
            }
            else if($userLevel == config::TEACHER_LEVEL)
            {
                $login->redirect('./pages/teacherMain.php');
            }
            else if($userLevel == config::ADMIN_LEVEL)
            {
                $login->redirect('./pages/adminMain.php');
            }
        }
        else
        {
            $error = "Blogai suvestas prisijungimo vardas arba slaptažodis!";
        }
    }
?>

<!DOCTYPE html>
<html>
<head>
    <title>Elektorninis Dienynas</title>
    <link rel="import" href="./includes/basicHeadInclude.html">
</head>
<body>

    <div class="container">
        <h1 class="text-center">Elektroninis dienynas</h1>
        <h3 class="text-center">Saulius Stankevičius IFF-4/2</h3>

        <form method="post">
            <div class="form-group">
                <label for="loginName">Prisijungimo vardas:</label>
                <input type="text" class="form-control" id="loginName" name="loginName" required="true"
                       oninvalid="this.setCustomValidity('Užpildykite šį lauką')" oninput="setCustomValidity('')" placeholder="Suveskite prisijungimo vardą">
            </div>
            <div class="form-group">
                <label for="pwd">Slaptažodis:</label>
                <input type="password" class="form-control" id="pwd" name="pwd" required="true"
                       oninvalid="this.setCustomValidity('Užpildykite šį lauką')" oninput="setCustomValidity('')" placeholder="Suveskite slaptažodį">
            </div>

            <div class="form-group">
            <input type="submit" class="btn btn-info" name="submit" value="Prisijunkti">

            <a href="registration.php" class="btn btn-link" role="button">Registracija</a>
            </div>

            <?php
            if ( isset($error) )
            {

                ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo $error ?>
                </div>
                <?php
            }
            ?>
        </form>

    </div>

</body>
</html>
