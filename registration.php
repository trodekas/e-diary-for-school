<?php
    session_start();
    require_once('./libraries/User.php');
    $user = new User();

    if($user->isLoggedIn()!="")
    {
        $userLevel = $user->getUserLevel($_SESSION['user_session']);

        if($userLevel == config::STUDENT_LEVEL)
        {
            $user->redirect('./pages/studentMain.php');
        }
        else if($userLevel == config::TEACHER_LEVEL)
        {
            $user->redirect('./pages/teacherMain.php');
        }
        else if($userLevel == config::ADMIN_LEVEL)
        {
            $user->redirect('./pages/adminMain.php');
        }
    }

    if(isset($_POST['submit']))
    {
        $uname = strip_tags($_POST['loginName']);
        $uregistrationCode = strip_tags($_POST['registrationCode']);
        $upass = strip_tags($_POST['pwd']);

        if(strlen($upass) < 6)
        {
            $error = "Slaptažodis turi būti bent 6 simbolių ilgio!";
        }
        else if(strlen($uname) < 6)
        {
            $error = "Prisjungimo vardas turi būti bent 6 simbolių ilgio!";
        }
        else if(strlen($uregistrationCode) < 10)
        {
            $error = "Registracijos kodas turi būti 10 simbolių ilgio!";
        }
        else
        {
            try
            {
                if($user->isUsernameTaken($uname))
                {
                    $error = "Šis vartotojo vardas yra užimtas!";
                }
                else if($user->isRegistrationCodeUsed($uregistrationCode))
                {
                    $error = "Šis registracijos kodas jau panaudotas!";
                }
                else
                {
                    if($user->register($uname, $uregistrationCode, $upass))
                    {
                        $success = "Registracija sėkminga";
                    }
                }
            }
            catch(PDOException $e)
            {
                echo $e->getMessage();
            }
        }
    }
?>

<!DOCTYPE html>
<html>
<head>
    <title>Registracija</title>
    <link rel="import" href="includes/basicHeadInclude.html">
</head>
<body>

<div class="container">
    <h1 class="text-center">Registracija</h1>

    <div id="loginForm">
    <form method="post" autocomplete="off" class="form-signin">

        <div class="form-group">
            <label for="registrationCode">Registracijos kodas:</label>
            <input type="text" class="form-control" id="registrationCode" name="registrationCode" required="true"
                   value="<?php if(isset($_POST['registrationCode'])) echo $_POST['registrationCode'];?>" maxlength="10"
                   oninvalid="this.setCustomValidity('Užpildykite šį lauką')" oninput="setCustomValidity('')" placeholder="Suveskite registracijos kodą">

        </div>

        <div class="form-group">
            <label for="loginName">Prisijungimo vardas:</label>
            <input type="text" class="form-control" name="loginName" id="loginName" required="true"
                   oninvalid="this.setCustomValidity('Užpildykite šį lauką')" oninput="setCustomValidity('')" placeholder="Suveskite prisijungimo vardą">
        </div>
        <div class="form-group">
            <label for="pwd">Slaptažodis:</label>
            <input type="password" class="form-control" name="pwd" id="pwd"  required="true"
                   oninvalid="this.setCustomValidity('Užpildykite šį lauką')" oninput="setCustomValidity('')" placeholder="Suveskite slaptažodį">
        </div>

        <div class="form-group">
        <input type="submit" class="btn btn-info" name="submit" id="btnSubmit" value="Registruotis">

        <a href="index.php" class="btn btn-link" role="button">Atgal</a>
        </div>

        <?php
        if ( isset($error) )
        {

            ?>
            <div class="alert alert-danger" role="alert">
                <?php echo $error ?>
            </div>
            <?php
        }
        else if( isset($success))
        {
            ?>
            <div class="alert alert-success" role="alert">
                <a href="index.php" class="btn btn-link" role="button">Registracija sėkminga spauskite čia norėdami prisijunkti</a>
            </div>
            <?php
        }
        ?>

    </form>
    </div>

</div>

</body>
</html>