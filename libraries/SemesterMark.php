<?php

require_once(__DIR__.'/../utils/Database.php');

class SemesterMark
{
    private $conn;

    public function __construct()
    {
        $database = new Database();
        $db = $database->connect();
        $this->conn = $db;
    }

    public function getSemesterMarkForStudentYearSubjectAndSemester($studentId, $year, $subjectId, $semesterId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM semestermarks WHERE fk_studentid='$studentId' AND fk_subjectid='$subjectId' AND YEAR(semestermarks.date)='$year' AND semestermarks.fk_semesterid='$semesterId'");
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function addSemesterMarkForStudentYearTeacherSubjectAndSemester($value, $studentId, $year, $teacherId, $subjectId, $semesterId)
    {
        try
        {
            if($semesterId == 1)
            {
                $date = $year."-12-12";
            }
            else
            {
                $date = $year."-05-05";
            }
            $stmt = $this->conn->prepare("INSERT INTO semestermarks(value, date, fk_studentid, fk_teacherid, fk_subjectid, fk_semesterid) VALUES ('$value', '$date', '$studentId', '$teacherId', '$subjectId', '$semesterId')");
            $stmt->execute();
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function checkIfStudentHasSemesterMarks($studentId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM semestermarks WHERE semestermarks.fk_studentid='$studentId'");
            $stmt->execute();

            return $stmt->rowCount() > 0;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }
}