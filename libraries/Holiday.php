<?php

require_once(__DIR__.'/../utils/Database.php');

class Holiday
{
    private $conn;

    public function __construct()
    {
        $database = new Database();
        $db = $database->connect();
        $this->conn = $db;
    }

    public function getHolidays($semester)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM holidays WHERE fk_semesterid=$semester");

            $stmt->execute();

            return $stmt;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }
}