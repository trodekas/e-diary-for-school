<?php

require_once(__DIR__.'/../utils/Database.php');

class Semester
{
    private $conn;

    public function __construct()
    {
        $database = new Database();
        $db = $database->connect();
        $this->conn = $db;
    }

    public function getAllSemesters()
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM semesters");
            $stmt->execute();

            return $stmt;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }
}