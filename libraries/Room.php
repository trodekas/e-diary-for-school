<?php

require_once(__DIR__.'/../utils/Database.php');

class Room
{
    private $conn;

    public function __construct()
    {
        $database = new Database();
        $db = $database->connect();
        $this->conn = $db;
    }

    public function checkIfRoomExists($roomName)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM rooms WHERE name='$roomName'");

            $stmt->execute();

            return $stmt->rowCount() > 0;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function checkIfRoomExistsIgnoreTeacherId($roomName, $teacherId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM rooms, teachers WHERE rooms.name='$roomName' AND teachers.fk_roomid=rooms.id AND teachers.id!='$teacherId'");

            $stmt->execute();

            return $stmt->rowCount() > 0;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getRoom($roomId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM rooms WHERE rooms.id='$roomId'");
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }
}