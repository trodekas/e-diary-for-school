<?php

require_once(__DIR__.'/../utils/Database.php');

class Marks
{
    private $conn;

    public function __construct()
    {
        $database = new Database();
        $db = $database->connect();
        $this->conn = $db;
    }

    public function getAllMarksForYearMonthAndStudent($year, $month, $studentId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM marks WHERE marks.fk_studentid='$studentId' AND YEAR(marks.date)='$year' AND MONTH(marks.date)='$month'");
            $stmt->execute();

            return $stmt;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getAverageForStudentSubjectYearAndSemester($subjectId, $studentId, $year, $semesterId)
    {
        try
        {
            if($semesterId == 1)
            {
                $stmt = $this->conn->prepare("SELECT ROUND(AVG(marks.value), 2) AS average FROM marks WHERE marks.fk_studentid='$studentId' AND marks.fk_subjectid='$subjectId' AND YEAR(marks.date)='$year' AND MONTH(marks.date) BETWEEN 9 AND 12");
                $stmt->execute();

                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                return $row;
            }
            else
            {
                $stmt = $this->conn->prepare("SELECT ROUND(AVG(marks.value), 2) AS average FROM marks WHERE marks.fk_studentid='$studentId' AND marks.fk_subjectid='$subjectId' AND YEAR(marks.date)='$year' AND MONTH(marks.date) BETWEEN 1 AND 5");
                $stmt->execute();

                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                return $row;
            }
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getRoundedAverageForStudentSubjectYearAndSemester($subjectId, $studentId, $year, $semesterId)
    {
        try
        {
            if($semesterId == 1)
            {
                $stmt = $this->conn->prepare("SELECT ROUND(AVG(marks.value), 0) AS average FROM marks WHERE marks.fk_studentid='$studentId' AND marks.fk_subjectid='$subjectId' AND YEAR(marks.date)='$year' AND MONTH(marks.date) BETWEEN 9 AND 12");
                $stmt->execute();

                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                return $row;
            }
            else
            {
                $stmt = $this->conn->prepare("SELECT ROUND(AVG(marks.value), 0) AS average FROM marks WHERE marks.fk_studentid='$studentId' AND marks.fk_subjectid='$subjectId' AND YEAR(marks.date)='$year' AND MONTH(marks.date) BETWEEN 1 AND 5");
                $stmt->execute();

                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                return $row;
            }
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getMarksForStudentTeacherAndDateRange($studentId, $teacherId, $dateFrom, $dateTo)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM marks WHERE fk_studentid='$studentId' AND fk_teacherid='$teacherId' AND marks.date BETWEEN '$dateFrom' AND '$dateTo'");
            $stmt->execute();

            return $stmt;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function addMarkForStudent($studentId, $teacherId, $summary, $date, $value, $subjectId)
    {
        try
        {
            $stmt = $this->conn->prepare("INSERT INTO marks(value, date, summary, fk_studentid, fk_subjectid, fk_teacherid) VALUES ('$value', '$date', '$summary', '$studentId', '$subjectId', '$teacherId')");
            $stmt->execute();
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function deleteMark($markId)
    {
        try
        {
            $stmt = $this->conn->prepare("DELETE FROM marks WHERE id='$markId'");
            $stmt->execute();
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getMark($markId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM marks WHERE id='$markId'");
            $stmt->execute();

            return $stmt->fetch(PDO::FETCH_ASSOC);
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function updateMark($value, $summary, $date, $markId)
    {
        try
        {
            $stmt = $this->conn->prepare("UPDATE marks SET marks.value='$value', marks.summary='$summary', marks.date='$date' WHERE marks.id='$markId'");
            $stmt->execute();
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function checkIfStudentHasMarks($studentId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM marks WHERE marks.fk_studentid='$studentId'");
            $stmt->execute();

            return $stmt->rowCount() > 0;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function checkIfStudentHasMarkForDateAndSubject($studentId, $date, $subjectId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM marks WHERE marks.fk_studentid='$studentId' AND marks.date='$date' AND marks.fk_subjectid='$subjectId'");
            $stmt->execute();

            return $stmt->rowCount() > 0;

        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }
}