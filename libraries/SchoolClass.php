<?php

require_once(__DIR__.'/../utils/Database.php');

class SchoolClass
{
    private $conn;

    public function __construct()
    {
        $database = new Database();
        $db = $database->connect();
        $this->conn = $db;
    }

    public function getClassForTeacher($teacherId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT students.name AS name, students.lastname AS lastname, classes.name AS classname FROM classes INNER JOIN students WHERE classes.fk_teacherid=$teacherId AND students.fk_classid=classes.id");

            $stmt->execute();

            return $stmt;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getClassStudentsForId($classId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT students.id AS studentid, students.name AS name, students.lastname AS lastname, classes.name AS classname FROM classes INNER JOIN students WHERE classes.id='$classId' AND students.fk_classid=classes.id");

            $stmt->execute();

            return $stmt;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getAllClasses()
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT classes.id AS classid, classes.name AS classname, teachers.name AS teachername, teachers.lastname AS teacherlastname FROM classes INNER JOIN teachers  WHERE teachers.id=classes.fk_teacherid");

            $stmt->execute();

            return $stmt;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getStudentCountInClass($classId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT COUNT(students.id) AS studentCount FROM students WHERE students.fk_classid = $classId");

            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function checkIfClassNameExist($className)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM classes WHERE classes.name='$className'");

            $stmt->execute();

            return $stmt->rowCount() > 0;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function addNewClass($className, $teacherId)
    {
        try
        {
            $stmt = $this->conn->prepare("INSERT INTO classes (name, fk_teacherid) VALUES ('$className', $teacherId)");

            $stmt -> execute();
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function deleteClass($classId)
    {
        try
        {
            $stmt = $this->conn->prepare("DELETE FROM scheduleclasses WHERE scheduleclasses.fk_classid='$classId'");
            $stmt->execute();

            $stmt = $this->conn->prepare("DELETE FROM classes WHERE classes.id='$classId'");
            $stmt->execute();
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getClass($classId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT classes.id AS classid, classes.name AS classname, teachers.name AS teachername, teachers.lastname AS teacherlastname, teachers.id AS teacherid FROM classes INNER JOIN teachers WHERE classes.id='$classId' AND classes.fk_teacherid=teachers.id");

            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function updateClassTeacher($classId, $teacherId)
    {
        try
        {
            $stmt = $this->conn->prepare("UPDATE classes SET classes.fk_teacherid='$teacherId' WHERE classes.id='$classId'");

            $stmt->execute();
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function checkIfTeacherHasAClass($teacherId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM classes WHERE fk_teacherid='$teacherId'");

            $stmt->execute();

            return $stmt->rowCount() > 0;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function checkIfClassExists($classId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM classes WHERE classes.id='$classId'");

            $stmt->execute();

            return $stmt->rowCount() > 0;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }
}