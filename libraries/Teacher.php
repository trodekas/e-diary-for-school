<?php

require_once(__DIR__.'/../utils/Database.php');

class Teacher
{
    private $conn;

    public function __construct()
    {
        $database = new Database();
        $db = $database->connect();
        $this->conn = $db;
    }

    public function getTeacher($userId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM teachers WHERE fk_userid=$userId");

            $stmt->execute();

            $userRow = $stmt->fetch(PDO::FETCH_ASSOC);

            return $userRow;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getTeacherForTeacherId($teacherId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM teachers WHERE  id='$teacherId'");

            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getAllTeachersWithoutClass()
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT teachers.id AS teacherid, teachers.name AS name, teachers.lastname AS lastname FROM teachers LEFT JOIN classes ON teachers.id = classes.fk_teacherid WHERE classes.fk_teacherid IS NULL");

            $stmt->execute();

            return $stmt;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function updateTeacher($teacherId, $teacherName, $teacherLastName, $teacherCode, $teacherSubject, $teacherRoom)
    {
        try
        {
            $stmt = $this->conn->prepare("UPDATE rooms SET rooms.name='$teacherRoom' WHERE rooms.id=(SELECT teachers.fk_roomid FROM teachers WHERE teachers.id='$teacherId')");
            $stmt->execute();

            $stmt = $this->conn->prepare("UPDATE teachers SET teachers.name='$teacherName', teachers.lastname='$teacherLastName', teachers.fk_subjectid='$teacherSubject', teachers.citizencode='$teacherCode' WHERE teachers.id='$teacherId'");
            $stmt->execute();
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getTeacherForSubject($subjectId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM teachers WHERE fk_subjectid='$subjectId'");
            $stmt->execute();

            return $stmt;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getTeacherForSubjectAndClass($subjectId, $classId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT teachers.id AS id, teachers.name AS name, teachers.lastname AS lastname FROM teachers INNER JOIN scheduleclasses WHERE scheduleclasses.fk_classid='$classId' AND scheduleclasses.fk_subjectid='$subjectId' AND teachers.id=scheduleclasses.fk_teacherid GROUP BY teachers.id");
            $stmt->execute();

            return $stmt;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }
}