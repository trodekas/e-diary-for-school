<?php

require_once(__DIR__.'/../utils/Database.php');

class Schedule
{
    private $conn;

    public function __construct()
    {
        $database = new Database();
        $db = $database->connect();
        $this->conn = $db;
    }

    public function getTodaysSubjectForClassAndTime($classId, $timeId)
    {
        try
        {
            setlocale(LC_ALL, 'lt-LT');
            $currentDay = date( "N", time());

            $stmt = $this->conn->prepare("SELECT subjects.name AS name FROM scheduleclasses INNER JOIN subjects, scheduledays WHERE scheduleclasses.fk_classid=$classId AND scheduleclasses.fk_timeid=$timeId AND subjects.id=scheduleclasses.fk_subjectid AND scheduleclasses.fk_dayid=scheduledays.id AND scheduledays.id=$currentDay");

            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getTodaysSubjectForTeacherAndTime($teacherId, $timeId)
    {
        try
        {
            setlocale(LC_ALL, 'lt-LT');
            $currentDay = date( "N", time());

            $stmt = $this->conn->prepare("SELECT DISTINCT subjects.name AS subjectname, classes.name AS classname FROM scheduleclasses INNER JOIN classes, scheduledays, subjects, teachers WHERE scheduleclasses.fk_teacherid=$teacherId AND scheduleclasses.fk_dayid=$currentDay AND scheduleclasses.fk_classid=classes.id AND scheduleclasses.fk_timeid=$timeId AND teachers.id=$teacherId AND teachers.fk_subjectid=subjects.id");

            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getSubjectForClassTimeAndDay($classId, $timeId, $dayId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT scheduledays.name AS dayname, subjects.name AS subjectname, rooms.name AS roomname, teachers.name AS teachername, teachers.lastname AS teacherlastname, classes.name AS classname FROM scheduleclasses INNER JOIN classes, teachers, subjects, rooms, scheduledays WHERE scheduleclasses.fk_classid=$classId AND scheduleclasses.fk_timeid=$timeId AND scheduleclasses.fk_subjectid=subjects.id AND scheduleclasses.fk_teacherid=teachers.id AND teachers.fk_roomid=rooms.id AND scheduleclasses.fk_dayid=$dayId AND scheduleclasses.fk_dayid=scheduledays.id");

            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getSubjectForTeacherTimeAndDay($teacherId, $timeId, $dayId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT scheduledays.name AS dayname, classes.name AS classname, subjects.name AS subjectname, rooms.name AS roomname FROM scheduleclasses, classes, subjects, rooms, teachers, scheduledays WHERE scheduleclasses.fk_teacherid=$teacherId AND scheduleclasses.fk_dayid=$dayId AND  scheduleclasses.fk_timeid=$timeId AND classes.id=scheduleclasses.fk_classid AND subjects.id=scheduleclasses.fk_subjectid AND teachers.id=$teacherId AND rooms.id=teachers.fk_roomid");

            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getAllScheduleTimes()
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT scheduletimes.id, DATE_FORMAT(scheduletimes.`from`, '%H:%i') AS fromTime, DATE_FORMAT(scheduletimes.`to`, '%H:%i') AS toTime FROM scheduletimes");

            $stmt->execute();

            return $stmt;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getAllScheduleDays()
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM scheduledays");

            $stmt->execute();

            return $stmt;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getClassListForTeacher($teacherId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT DISTINCT scheduleclasses.fk_classid, classes.name FROM scheduleclasses INNER JOIN classes WHERE scheduleclasses.fk_teacherid=$teacherId AND scheduleclasses.fk_classid=classes.id");

            $stmt->execute();

            return $stmt;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getFullScheduleForClass($classId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT scheduleclasses.id AS scheduleclassid, subjects.id AS subjectid, subjects.name AS subjectname, teachers.id AS teacherid, teachers.name AS teachername, teachers.lastname AS teacherlastname, rooms.id AS roomid, rooms.name AS roomname, scheduledays.id AS dayid, scheduledays.name AS dayname, scheduletimes.id AS timeid, DATE_FORMAT(scheduletimes.`from`, '%H:%i') AS timefrom, DATE_FORMAT(scheduletimes.`to`, '%H:%i') AS timeto FROM scheduleclasses INNER JOIN scheduletimes, teachers, rooms, subjects, scheduledays WHERE scheduleclasses.fk_classid='$classId' AND scheduleclasses.fk_teacherid=teachers.id AND teachers.fk_roomid=rooms.id AND scheduleclasses.fk_dayid=scheduledays.id AND scheduleclasses.fk_timeid=scheduletimes.id AND scheduleclasses.fk_subjectid=subjects.id");
            $stmt->execute();

            return $stmt;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function deleteScheduleClass($scheduleclassId)
    {
        try
        {
            $stmt = $this->conn->prepare("DELETE FROM scheduleclasses WHERE id='$scheduleclassId'");
            $stmt->execute();
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function checkIfClassHasScheduleClassForSubject($classId, $subjectId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM scheduleclasses WHERE fk_subjectid='$subjectId' AND fk_classid='$classId'");
            $stmt->execute();

            return $stmt->rowCount() > 0;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function checkIfClassExistsForTeacherDayAndTime($teacherId, $dayId, $timeId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM scheduleclasses WHERE fk_teacherid='$teacherId' AND fk_dayid='$dayId' AND fk_timeid='$timeId'");
            $stmt->execute();

            return $stmt->rowCount() > 0;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function checkIfClassExistsForClassDayAndTime($classId, $dayId, $timeId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM scheduleclasses WHERE fk_classid='$classId' AND fk_dayid='$dayId' AND fk_timeid='$timeId'");
            $stmt->execute();

            return $stmt->rowCount() > 0;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function addNewScheduleClass($subjectId, $dayId, $timeId, $classId, $teacherId)
    {
        try
        {
            $stmt = $this->conn->prepare("INSERT INTO scheduleclasses(fk_subjectid, fk_dayid, fk_timeid, fk_classid, fk_teacherid) VALUES ('$subjectId', '$dayId', '$timeId', '$classId', '$teacherId')");
            $stmt->execute();
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function checkIfTeachesHasScheduleClass($teacherId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM scheduleclasses WHERE fk_teacherid='$teacherId'");
            $stmt->execute();

            return $stmt->rowCount() > 0;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getScheduleClass($scheduleId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM scheduleclasses WHERE id='$scheduleId'");
            $stmt->execute();

            return $stmt->fetch(PDO::FETCH_ASSOC);
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function updateScheduleClass($scheduleId, $timeId, $dayId)
    {
        try
        {
            $stmt = $this->conn->prepare("UPDATE scheduleclasses SET fk_dayid='$dayId',fk_timeid='$timeId' WHERE scheduleclasses.id='$scheduleId'");
            $stmt->execute();
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }
}