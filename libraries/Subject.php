<?php

require_once(__DIR__.'/../utils/Database.php');

class Subject
{
    private $conn;

    public function __construct()
    {
        $database = new Database();
        $db = $database->connect();
        $this->conn = $db;
    }

    public function getAllSubjects()
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM subjects ORDER BY id");

            $stmt->execute();

            return $stmt;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function checkIfSubjectExists($subjectId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM subjects WHERE subjects.id='$subjectId'");
            $stmt->execute();

            return $stmt->rowCount() > 0;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getSubject($subjectId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM subjects WHERE id='$subjectId'");
            $stmt->execute();

            return $stmt->fetch(PDO::FETCH_ASSOC);
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }
}