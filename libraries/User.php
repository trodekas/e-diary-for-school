<?php

require_once(__DIR__.'/../utils/Database.php');

class User
{

    private $conn;

    public function __construct()
    {
        $database = new Database();
        $db = $database->connect();
        $this->conn = $db;
    }

    public function runQuery($sql)
    {
        $stmt = $this->conn->prepare($sql);

        return $stmt;
    }

    public function isRegistrationCodeUsed($uregistrationCode)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT username FROM users WHERE registrationcode=:uregistrationCode AND username IS NOT NULL");
            $stmt->bindParam(":uregistrationCode", $uregistrationCode);

            $stmt->execute();

            return $stmt->rowCount() > 0;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function isUsernameTaken($uname)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT username FROM users WHERE username=:uname");
            $stmt->bindParam(":uname", $uname);

            $stmt->execute();

            return $stmt->rowCount() == 1;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function register($uname, $uregistrationCode, $upass)
    {
        try
        {
            $new_password = password_hash($upass, PASSWORD_DEFAULT);

            $stmt = $this->conn->prepare("UPDATE users SET username=:uname, password=:upass WHERE registrationcode=:uregistrationCode");

            $stmt->bindparam(":uname", $uname);
            $stmt->bindparam(":uregistrationCode", $uregistrationCode);
            $stmt->bindparam(":upass", $new_password);

            $stmt->execute();

            return $stmt;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }


    public function doLogin($uname, $upass)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT id, username, password FROM users WHERE username=:uname");
            $stmt->bindparam(':uname', $uname);
            $stmt->execute();
            $userRow=$stmt->fetch(PDO::FETCH_ASSOC);
            if($stmt->rowCount() == 1)
            {
                if(password_verify($upass, $userRow['password']))
                {
                    $_SESSION['user_session'] = $userRow['id'];

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function isLoggedIn()
    {
        if(isset($_SESSION['user_session']))
        {
            return true;
        }
    }

    public function getUserLevel($id)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT userlevel FROM users WHERE id=:id");
            $stmt->bindParam(":id", $id);

            $stmt->execute();

            $results = $stmt->fetch(PDO::FETCH_ASSOC);
            return $results['userlevel'];
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function redirect($url)
    {
        header("Location: $url");
    }

    public function doLogout()
    {
        session_destroy();
        unset($_SESSION['user_session']);

        return true;
    }

    private function generateRandomString()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 10; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    private function checkIfRandomStringExists($string)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM users WHERE users.registrationcode='$string'");

            $stmt->execute();

            return $stmt->rowCount() > 0;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function checkIfCitizenCodeExists($citizenCode)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM students, teachers WHERE students.citizencode='$citizenCode' OR teachers.citizencode='$citizenCode'");

            $stmt->execute();

            return $stmt->rowCount() > 0;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function checkIfCitizenCodeExistsIgnoreStudentWithId($citizenCode, $studentId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM students WHERE citizencode='$citizenCode' AND id!='$studentId'");
            $stmt->execute();

            if($stmt->rowCount() > 0)
            {
                return true;
            }

            $stmt = $this->conn->prepare("SELECT * FROM teachers WHERE citizencode='$citizenCode'");
            $stmt->execute();

            return $stmt->rowCount() > 0;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function checkIfCitizenCodeExistsIgnoreTeacherWithId($citizenCode, $teacherId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM teachers WHERE citizencode='$citizenCode' AND id!='$teacherId'");
            $stmt->execute();

            if($stmt->rowCount() > 0)
            {
                return true;
            }

            $stmt = $this->conn->prepare("SELECT * FROM students WHERE citizencode='$citizenCode'");
            $stmt->execute();

            return $stmt->rowCount() > 0;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function addNewStudentToSystem($name, $lastname, $citizenCode, $classId)
    {
        try
        {
            $registrationCode = $this->generateRandomString();
            while($this->checkIfRandomStringExists($registrationCode))
            {
                $registrationCode = $this->generateRandomString();
            }
            $userLevel = config::STUDENT_LEVEL;

            $stmt = $this->conn->prepare("INSERT INTO users (users.registrationcode, users.userlevel) VALUES ('$registrationCode', '$userLevel')");

            $stmt->execute();
            $lastInsertedId = $this->conn->lastInsertId();

            $stmt = $this->conn->prepare("INSERT INTO students (students.citizencode, students.name, students.lastname, students.fk_userid, students.fk_classid) VALUES ('$citizenCode', '$name', '$lastname', '$lastInsertedId', '$classId')");

            $stmt->execute();
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getAllStudents()
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT classes.name AS classname, students.id AS studentid, users.id AS userid, users.registrationcode, students.name, students.lastname, students.citizencode FROM users INNER JOIN students, classes WHERE users.id = students.fk_userid AND students.fk_classid=classes.id");

            $stmt->execute();

            return $stmt;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function deleteStudentFromSystem($userId)
    {
        try
        {
            $stmt = $this->conn->prepare("DELETE FROM students WHERE students.fk_userid='$userId'");
            $stmt->execute();

            $stmt = $this->conn->prepare("DELETE FROM users WHERE id='$userId'");
            $stmt->execute();
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getAllTeachers()
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT teachers.id AS teacherid, users.id AS userid, teachers.citizencode, teachers.name, teachers.lastname, users.registrationcode, rooms.name AS roomname, subjects.name AS subjectname FROM users INNER JOIN teachers, rooms, subjects WHERE users.id=teachers.fk_userid AND teachers.fk_userid=users.id AND teachers.fk_subjectid=subjects.id AND teachers.fk_roomid=rooms.id");

            $stmt->execute();

            return $stmt;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function addNewTeacherToSystem($name, $lastname, $citizenCode, $roomName, $subjectId)
    {
        try
        {
            $registrationCode = $this->generateRandomString();
            while($this->checkIfRandomStringExists($registrationCode))
            {
                $registrationCode = $this->generateRandomString();
            }
            $userLevel = config::TEACHER_LEVEL;

            $stmt = $this->conn->prepare("INSERT INTO users (users.registrationcode, users.userlevel) VALUES ('$registrationCode', '$userLevel')");

            $stmt->execute();
            $lastInsertedId = $this->conn->lastInsertId();

            $stmt = $this->conn->prepare("INSERT INTO rooms (rooms.name) VALUES ('$roomName')");
            $stmt->execute();
            $lastInsertedRoomId = $this->conn->lastInsertId();

            $stmt = $this->conn->prepare("INSERT INTO teachers (teachers.citizencode, teachers.name, teachers.lastname, teachers.fk_userid, teachers.fk_subjectid, teachers.fk_roomid) VALUES ('$citizenCode', '$name', '$lastname', '$lastInsertedId', '$subjectId', '$lastInsertedRoomId')");

            $stmt->execute();
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function deleteTeacherFromSystem($userId)
    {
        try
        {
            $stmt = $this->conn->prepare("DELETE FROM homeworks WHERE fk_teacherid=(SELECT teachers.fk_roomid FROM teachers WHERE teachers.fk_userid='$userId')");
            $stmt->execute();

            $stmt = $this->conn->prepare("SELECT * FROM rooms WHERE id=(SELECT teachers.fk_roomid FROM teachers WHERE teachers.fk_userid='$userId')");
            $stmt->execute();
            $rommRow = $stmt->fetch(PDO::FETCH_ASSOC);
            $roomId = $rommRow['id'];

            $stmt = $this->conn->prepare("DELETE FROM teachers WHERE teachers.fk_userid='$userId'");
            $stmt->execute();

            $stmt = $this->conn->prepare("DELETE FROM users WHERE id='$userId'");
            $stmt->execute();

            $stmt = $this->conn->prepare("DELETE FROM rooms WHERE rooms.id='$roomId'");
            $stmt->execute();
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }
}