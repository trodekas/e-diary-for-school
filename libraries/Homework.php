<?php

require_once(__DIR__.'/../utils/Database.php');

class Homework
{
    private $conn;

    public function __construct()
    {
        $database = new Database();
        $db = $database->connect();
        $this->conn = $db;
    }

    public function getHomeworksForClass($classId)
    {
        try
        {
            setlocale(LC_ALL, 'lt-LT');
            $currentDate = date("Y-m-d");

            $stmt = $this->conn->prepare("SELECT homeworks.start, homeworks.end, homeworks.summary, subjects.name FROM homeworks INNER JOIN subjects, teachers WHERE homeworks.fk_classid=$classId AND homeworks.fk_teacherid=teachers.id AND teachers.fk_subjectid=subjects.id AND homeworks.end >= '$currentDate' ORDER BY homeworks.end");

            $stmt->execute();

            return $stmt;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function addHomeworkForClass($classId, $teacherId, $toDate, $summary)
    {
        try
        {
            setlocale(LC_ALL, 'lt-LT');
            $currentDate = date("Y-m-d");

            $stmt = $this->conn->prepare("INSERT INTO homeworks (summary, start, end, fk_classid, fk_teacherid) VALUES ('$summary', '$currentDate', '$toDate', $classId, $teacherId)");

            $stmt->execute();
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getHomeworksForTeacherClassAndDateRange($teacherId, $classId, $dateFrom, $dateTo)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM homeworks WHERE homeworks.fk_classid=$classId AND homeworks.fk_teacherid=$teacherId AND homeworks.end BETWEEN '$dateFrom' AND '$dateTo'");

            $stmt->execute();

            return $stmt;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function deleteHomework($homeworkId)
    {
        try
        {
            $stmt = $this->conn->prepare("DELETE FROM homeworks WHERE id='$homeworkId'");

            $stmt->execute();
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getHomework($homeworkId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM homeworks WHERE id='$homeworkId'");

            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function updateHomework($classId, $toDate, $summary, $homeworkId)
    {
        try
        {
            $stmt = $this->conn->prepare("UPDATE homeworks SET homeworks.fk_classid='$classId', homeworks.end='$toDate', homeworks.summary='$summary' WHERE homeworks.id='$homeworkId'");

            $stmt->execute();
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }
}