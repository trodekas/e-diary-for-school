<?php

require_once(__DIR__.'/../utils/Database.php');

class Report
{
    private $conn;

    public function __construct()
    {
        $database = new Database();
        $db = $database->connect();
        $this->conn = $db;
    }

    public function getReportOfTheDay()
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT report FROM reportoftheday WHERE id=(SELECT MAX(id) FROM reportoftheday)");

            $stmt->execute();

            $userRow = $stmt->fetch(PDO::FETCH_ASSOC);

            return $userRow['report'];
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function updateReportOfTheDay($newReport)
    {
        try
        {
            $stmt = $this->conn->prepare("UPDATE reportoftheday SET report='$newReport'");

            $stmt->execute();
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }
}