<?php

require_once(__DIR__.'/../utils/Database.php');

class Student
{
    private $conn;

    public function __construct()
    {
        $database = new Database();
        $db = $database->connect();
        $this->conn = $db;
    }

    public function getStudent($userId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM students WHERE fk_userid=$userId");

            $stmt->execute();

            $userRow = $stmt->fetch(PDO::FETCH_ASSOC);

            return $userRow;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getStudentForStudentId($studentId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM students WHERE  id='$studentId'");

            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getAllStudents()
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM students");

            $stmt->execute();

            return $stmt;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getAllStudentNotInClass($classId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM students WHERE fk_classid!='$classId'");

            $stmt->execute();

            return $stmt;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getAllStudentsForClass($classId)
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM students WHERE students.fk_classid='$classId'");

            $stmt->execute();

            return $stmt;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function updateStudentClass($studentId, $classId)
    {
        try
        {
            $stmt = $this->conn->prepare("UPDATE students SET students.fk_classid='$classId' WHERE students.id='$studentId'");

            $stmt->execute();
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function updateStudent($studentName, $studentLastName, $studentCode, $studentId)
    {
        try
        {
            $stmt = $this->conn->prepare("UPDATE students SET students.name='$studentName', students.lastname='$studentLastName', students.citizencode='$studentCode' WHERE students.id='$studentId'");

            $stmt->execute();
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }
}