<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand">Mokytojo Dienynas</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="../pages/teacherMain.php">Pragrindinis</a></li>
            <li><a href="../pages/teacherMyClass.php">Klasė</a></li>
            <li><a href="../pages/teacherSchedule.php">Tvarkaraštis</a></li>
            <li><a href="../pages/teacherHomework.php">Namų darbai</a></li>
            <li><a href="../pages/teacherMark.php">Pažymiai</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="../utils/logout.php?logout=true">Atsijunkti</a></li>
        </ul>
    </div>
</nav>