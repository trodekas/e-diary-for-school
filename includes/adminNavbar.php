<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand">Administratoriaus Dienynas</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="../pages/adminMain.php">Pagrindinis</a></li>
            <li><a href="../pages/adminClass.php">Klasės</a></li>
            <li><a href="../pages/adminUser.php">Vartotojai</a></li>
            <li><a href="../pages/adminSchedule.php">Tvarkaraštis</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="../utils/logout.php?logout=true">Atsijunkti</a></li>
        </ul>
    </div>
</nav>