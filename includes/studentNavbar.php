<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand">Moksleivio Dienynas</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="../pages/studentMain.php">Pagrindinis</a></li>
            <li><a href="../pages/studentHomework.php">Namų darbai</a></li>
            <li><a href="../pages/studentHoliday.php">Atostogos</a></li>
            <li><a href="../pages/studentSchedule.php">Tvarkaraštis</a></li>
            <li><a href="../pages/studentMark.php">Pažymiai</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="../utils/logout.php?logout=true">Atsijunkti</a></li>
        </ul>
    </div>
</nav>