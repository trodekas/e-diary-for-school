<?php
    require_once("../utils/teacherSession.php");

    require_once("../libraries/User.php");
    $auth_user = new User();

    $user_id = $_SESSION['user_session'];

    $stmt = $auth_user->runQuery("SELECT * FROM users WHERE id=:user_id");
    $stmt->execute(array(":user_id"=>$user_id));

    $userRow=$stmt->fetch(PDO::FETCH_ASSOC);

    require_once("../libraries/Teacher.php");
    $teacher = new Teacher();

    $teacherInfo = $teacher->getTeacher($user_id);

    require_once("../libraries/Schedule.php");
    $schedule = new Schedule();
    $teachersClasses = $schedule->getClassListForTeacher($teacherInfo['id']);
    if($teachersClasses != null)
    {
        $teachersClasses = $teachersClasses->fetchAll();
    }

    require_once("../libraries/Homework.php");
    $homework = new Homework();

    if(isset($_POST['submit']))
    {
        if(!isset($_POST['schoolClass']))
        {
            $errorNew = "Nėra klasės";
        }
        else {
            $homeworkSummary = $_POST['summary'];
            $homeworkClass = $_POST['schoolClass'];
            $homeworkDate = $_POST['toDate'];

            if (strlen($homeworkSummary) < 1) {
                $errorNew = "Aprašymas turi būti užpildytas";
            } else if (strlen($homeworkDate) < 1) {
                $errorNew = "Data turi būti užpildyta";
            } else {
                $homework->addHomeworkForClass($homeworkClass, $teacherInfo['id'], $homeworkDate, $homeworkSummary);
                $successNew = "Namų darbas sėkmingai įtrauktas";
            }
        }
    }

    if(isset($_POST['submitShow']))
    {
        if(!isset($_POST['schoolClassFind']))
        {
            $errorFind = "Nėra klasės";
        }
        else {
            $homeworkClass = $_POST['schoolClassFind'];
            $homeworkFromDate = $_POST['fromDateFind'];
            $homeworkToDate = $_POST['toDateFind'];

            if (strlen($homeworkFromDate) < 1) {
                $errorFind = "Data turi būti nurodyta";
            } else if (strlen($homeworkToDate) < 1) {
                $errorFind = "Data turi būti nurodyta";
            } else {
                $homeworksInRange = $homework->getHomeworksForTeacherClassAndDateRange($teacherInfo['id'], $homeworkClass, $homeworkFromDate, $homeworkToDate);
                if ($homeworksInRange != null) {
                    $homeworksInRange = $homeworksInRange->fetchAll();
                }
            }
        }
    }

    if(isset($_POST['submitDelete']))
    {
        $homework->deleteHomework($_POST['deleteId']);

        $successDel = "Namų darbas ištrintas";
    }
?>

<!DOCTYPE html>
<html>
<head>
    <title>Namų darbai</title>
    <link rel="import" href="../includes/basicHeadInclude.html">
</head>
<body>
<?php include_once '../includes/teacherNavbar.php'; ?>

    <div class="container">
        <h3>Naujas namų darbas</h3>

       <div class="row">
           <form method="post">
               <div class="form-group col-lg-3">
                   <label for="schoolClass">Klasė:</label>
                   <select class="form-control" id="schoolClass" name="schoolClass">
                       <?php
                            foreach ($teachersClasses as $class)
                            {
                                ?><option value="<?php echo $class['fk_classid']; ?>"><?php echo $class['name']; ?></option><?php
                            }
                       ?>
                   </select>
               </div>
               <div class="form-group col-lg-3">
                   <label for="toDate">Iki:</label>
                   <input type="date" class="form-control" id="toDate" name="toDate" min="<?php echo date("Y-m-d"); ?>">
               </div>
               <div class="form-group col-lg-6">
                   <label for="summary">Aprašas:</label>
                   <input type="text" class="form-control" id="summary" name="summary">
               </div>
               <div class="form-group col-lg-4">
                   <input type="submit" class="btn btn-info" name="submit" value="Įvesti">
               </div>
           </form>
       </div>

        <?php
        if ( isset($errorNew) )
        {

            ?>
            <div class="alert alert-danger" role="alert">
                <?php echo $errorNew ?>
            </div>
            <?php
        }
        else if ( isset($successNew) )
        {

            ?>
            <div class="alert alert-success" role="alert">
                <?php echo $successNew ?>
            </div>
            <?php
        }
        ?>

        <h3>Rodyti namų darbus</h3>

        <div class="row">
            <form method="post">
                <div class="form-group col-lg-3">
                    <label for="schoolClassFind">Klasė:</label>
                    <select class="form-control" id="schoolClassFind" name="schoolClassFind">
                        <?php
                        foreach ($teachersClasses as $class)
                        {
                            ?><option <?php if(isset($_POST['schoolClassFind']) && $class['fk_classid'] == $_POST['schoolClassFind']) echo "selected"; ?> value="<?php echo $class['fk_classid']; ?>"><?php echo $class['name']; ?></option><?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group col-lg-3">
                    <label for="fromDateFind">Nuo:</label>
                    <input type="date" class="form-control" id="fromDateFind" name="fromDateFind">
                </div>
                <div class="form-group col-lg-3">
                    <label for="toDateFind">Iki:</label>
                    <input type="date" class="form-control" id="toDateFind" name="toDateFind">
                </div>
                <div class="form-group col-lg-4">
                    <input type="submit" class="btn btn-info" name="submitShow" value="Rodyti">
                </div>
            </form>
        </div>

        <?php
        if ( isset($errorFind) )
        {

            ?>
            <div class="alert alert-danger" role="alert">
                <?php echo $errorFind ?>
            </div>
            <?php
        }
        ?>

        <table class="table table-hover">

            <thead>
            <tr>
                <th>#</th>
                <th>Aprašas</th>
                <th>Nuo</th>
                <th>Iki</th>
                <th>Veiksmai</th>
            </tr>
            </thead>

            <tbody>
            <?php
            if(isset($homeworksInRange))
            {
                $counter = 1;
                foreach ($homeworksInRange as $hm)
                {
                    ?><tr><?php
                    ?><td><?php echo $counter; ?></td><?php
                    ?><td><?php echo $hm['summary']; ?></td><?php
                    ?><td><?php echo $hm['start']; ?></td><?php
                    ?><td><?php echo $hm['end']; ?></td><?php

                    ?><td class="homework-edit">
                    <form method="post">
                    <input type="hidden" name="deleteId" value="<?php echo $hm['id']; ?>">
                    <input type="submit" class="btn btn-link" name="submitDelete" value="Trinti">
                        <a href="teacherHomeworkModification.php?modificationId=<?php echo $hm['id']?>" class="btn btn-link" role="button">Redaguoti</a>
                    </form>
                    </td><?php

                    ?></tr><?php

                    $counter++;
                }
            }
            ?>
            </tbody>
        </table>

        <?php
        if ( isset($successDel) )
        {

            ?>
            <div class="alert alert-success" role="alert">
                <?php echo $successDel ?>
            </div>
            <?php
        }
        ?>

    </div>


</body>
</html>
