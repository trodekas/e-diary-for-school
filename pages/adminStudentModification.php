<?php
require_once("../utils/adminSession.php");

require_once("../libraries/User.php");
$auth_user = new User();

$user_id = $_SESSION['user_session'];

$stmt = $auth_user->runQuery("SELECT * FROM users WHERE id=:user_id");
$stmt->execute(array(":user_id"=>$user_id));

$userRow=$stmt->fetch(PDO::FETCH_ASSOC);

require_once("../libraries/Student.php");
$student = new Student();
$currentStudent = $student->getStudentForStudentId($_GET['modificationId']);

if(isset($_POST['submit']))
{
    $studentName = $_POST['studentName'];
    $studentLastName = $_POST['studentLastName'];
    $studentCode = $_POST['studentCode'];

    if(strlen($studentName) < 1)
    {
        $errorNew = "Neįvestas moksleivio vardas";
    }
    else if(strlen($studentLastName) < 1)
    {
        $errorNew = "Neįvesta moksleivio pavardė";
    }
    else if(strlen($studentCode) < 11)
    {
        $errorNew = "Blogai suvestas asmens kodas";
    }
    else if($auth_user->checkIfCitizenCodeExistsIgnoreStudentWithId($studentCode, $_GET['modificationId']))
    {
        $errorNew = "Toks asmens kodas jau egzistuoja";
    }
    else
    {
        $student->updateStudent($studentName, $studentLastName, $studentCode, $_GET['modificationId']);
        $successNew = "Moksleivis sėkmingai modifikuotas";
        $currentStudent = $student->getStudentForStudentId($_GET['modificationId']);
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Moksleivio modifikavimas</title>
    <link rel="import" href="../includes/basicHeadInclude.html">
</head>
<body>
<?php include_once '../includes/adminNavbar.php'; ?>

<div class="container">
    <h3>Redaguoti moksleivį</h3>

    <div class="row">
        <form method="post">
            <div class="form-group col-lg-4">
                <label for="studentName">Vardas:</label>
                <input type="text" class="form-control" id="studentName" name="studentName" value="<?php echo $currentStudent['name']?>" minlength="1" pattern="^[a-zą-žA-ZĄ-Ž]+$" oninvalid="this.setCustomValidity('Neteisingai užpildytas vardas')" oninput="setCustomValidity('')">
            </div>
            <div class="form-group col-lg-4">
                <label for="studentLastName">Pavardė:</label>
                <input type="text" class="form-control" id="studentLastName" name="studentLastName" value="<?php echo $currentStudent['lastname']?>" minlength="1" pattern="^[a-zą-žA-ZĄ-Ž]+$" oninvalid="this.setCustomValidity('Neteisingai užpildyta pavardė')" oninput="setCustomValidity('')">
            </div>
            <div class="form-group col-lg-4">
                <label for="studentCode">Asmens kodas:</label>
                <input type="text" class="form-control" id="studentCode" name="studentCode" value="<?php echo $currentStudent['citizencode']?>" minlength="11" maxlength="11" pattern="^\d{11}$" oninvalid="this.setCustomValidity('Neteisingai užpildytas asmens kodas')" oninput="setCustomValidity('')">
            </div>
            <div class="form-group col-lg-4">
                <input type="submit" class="btn btn-info" name="submit" value="Išsaugoti">
            </div>

        </form>
    </div>

    <?php
    if ( isset($errorNew) )
    {

        ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $errorNew ?>
        </div>
        <?php
    }
    else if ( isset($successNew) )
    {

        ?>
        <div class="alert alert-success" role="alert">
            <?php echo $successNew ?>
        </div>
        <?php
    }
    ?>

</div>
</body>
</html>

