<?php
    require_once("../utils/studentSession.php");

    require_once("../libraries/User.php");
    $auth_user = new User();

    $user_id = $_SESSION['user_session'];

    $stmt = $auth_user->runQuery("SELECT * FROM users WHERE id=:user_id");
    $stmt->execute(array(":user_id"=>$user_id));

    $userRow=$stmt->fetch(PDO::FETCH_ASSOC);

    require_once("../libraries/Student.php");
    $student = new Student();
    $studentInfo = $student->getStudent($user_id);

    require_once("../libraries/Homework.php");
    $homework = new Homework();
    $homeworkInfo = $homework->getHomeworksForClass($studentInfo['fk_classid']);
?>

<!DOCTYPE html>
<html>
<head>
    <title>Atostogos</title>
    <link rel="import" href="../includes/basicHeadInclude.html">
</head>
<body>
<?php include_once '../includes/studentNavbar.php'; ?>

<div class="container">
        <div>
            <h2>Namų darbai</h2>

            <table class="table table-hover">

                <thead>
                <tr>
                    <th>#</th>
                    <th>Pamoka</th>
                    <th>Nuo</th>
                    <th>Iki</th>
                    <th>Aprašymas</th>
                </tr>
                </thead>

                <tbody>
                <?php
                $counter = 1;
                while($homeworkItem = $homeworkInfo->fetch(PDO::FETCH_ASSOC))
                {
                    ?><tr><?php
                    ?><td><?php echo $counter; ?></td><?php
                    ?><td><?php echo $homeworkItem['name']; ?></td><?php
                    ?><td><?php echo $homeworkItem['start']; ?></td><?php
                    ?><td><?php echo $homeworkItem['end']; ?></td><?php
                    ?><td><?php echo $homeworkItem['summary']; ?></td><?php
                    ?></tr><?php

                    $counter++;
                }
                ?>
                </tbody>

            </table>
        </div>
</div>

</body>
</html>
