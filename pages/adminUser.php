<?php
require_once("../utils/Patterns.php");
require_once("../utils/adminSession.php");

require_once("../libraries/User.php");
$auth_user = new User();

$user_id = $_SESSION['user_session'];

$stmt = $auth_user->runQuery("SELECT * FROM users WHERE id=:user_id");
$stmt->execute(array(":user_id"=>$user_id));

$userRow=$stmt->fetch(PDO::FETCH_ASSOC);

require_once ("../libraries/SchoolClass.php");
$schoolClass = new SchoolClass();

require_once("../libraries/Room.php");
$room = new Room();

require_once("../libraries/Schedule.php");
$schedule = new Schedule();

require_once("../libraries/Marks.php");
$marks = new Marks();

require_once("../libraries/SemesterMark.php");
$semesterMarks = new SemesterMark();

require_once("../libraries/Student.php");
$student = new Student();

require_once("../libraries/Teacher.php");
$teacher = new Teacher();

if(isset($_POST['submitNewStudent']))
{
    $studentName = $_POST['newStudentName'];
    $studenLastName = $_POST['newStudentLastName'];
    $studentCode = $_POST['newStudentCode'];
    $studenClass = $_POST['schoolClass'];

    if(strlen($studentName) < 1)
    {
        $errorNew = "Užpildykite moksleivio vardą";
    }
    else if(strlen($studenLastName) < 1)
    {
        $errorNew = "Užpildykite moksleivio pavardę";
    }
    else if($auth_user->checkIfCitizenCodeExists($studentCode))
    {
        $errorNew = "Toks asmens kodas jau egzistuoja";
    }
    else if($studenClass == "")
    {
        $errorNew = "Nenurodyta klasė";
    }
    else
    {
        $auth_user->addNewStudentToSystem($studentName, $studenLastName, $studentCode, $studenClass);

        $successNew = "Moksleivis sėkmingai pridėtas";
    }
}
else if(isset($_POST['submitDeleteStudent']))
{
    $studentForDelete = $student->getStudent($_POST['deleteId']);


    if($studentForDelete != null && $marks->checkIfStudentHasMarks($studentForDelete['id']))
    {
        $errorNew = "Negalima ištrinti, nes moksleivis turi pažymių";
    }
    else if($studentForDelete != null && $semesterMarks->checkIfStudentHasSemesterMarks($studentForDelete['id']))
    {
        $errorNew = "Negalima ištrinti, nes moksleivis turi pažymių";
    }
    else
    {
        $auth_user->deleteStudentFromSystem($_POST['deleteId']);

        $successNew = "Moksleivis sėkmingai pašalintas";
    }
}

if(isset($_POST['submitNewTeacher']))
{
    $teacherName = $_POST['newTeacherName'];
    $teacherLastName = $_POST['newTeacherLastName'];
    $teacherCode = $_POST['newTeacherCode'];
    $teacherRoom = $_POST['newTeacherRoom'];
    $teacherSubject = $_POST['subject'];

    if($teacherSubject == "")
    {
        $errorNewTeacher = "Nepasirinktas mokytojas";
    }
    else if(strlen($teacherName) < 1)
    {
        $errorNewTeacher = "Neįrašytas mokytojo vardas";
    }
    else if(strlen($teacherLastName) < 1)
    {
        $errorNewTeacher = "Neįrašyta mokytojo pavardė";
    }
    else if(strlen($teacherRoom) < 3)
    {
        $errorNewTeacher = "Klaidingas kabinetas";
    }
    else if($room->checkIfRoomExists($teacherRoom))
    {
        $errorNewTeacher = "Kabinetas užimtas";
    }
    else if($auth_user->checkIfCitizenCodeExists($teacherCode))
    {
        $errorNewTeacher = "Toks asmens kodas jau yra";
    }
    else
    {
        $auth_user->addNewTeacherToSystem($teacherName, $teacherLastName, $teacherCode, $teacherRoom, $teacherSubject);

        $successNewTeacher = "Mokytojas sėkmingai pridėtas";
    }
}
else if(isset($_POST['submitDeleteTeacher']))
{
    $teacherDeleteId = $_POST['deleteTeacherId'];
    $teacherDeleteTrueId = $_POST['deleteTeacherTrueId'];

    if($schoolClass->checkIfTeacherHasAClass($teacherDeleteTrueId))
    {
        $errorNewTeacher = "Negalima pašalinti mokytojo, nes jis turi klasę";
    }
    else if($schedule->checkIfTeachesHasScheduleClass($teacherDeleteTrueId))
    {
        $errorNewTeacher = "Negalima pašalinti mokytojo, nes jis turi mokomas klases";
    }
    else
    {
        $auth_user->deleteTeacherFromSystem($teacherDeleteId);

        $successNewTeacher = "Mokytojas pašalintas sėkmingai";
    }
}

require_once("../libraries/Subject.php");
$subject = new Subject();

if(isset($_POST['submitNewTeacherFile']))
{
    $csv_file =  $_FILES['csv_file']['tmp_name'];
    if (is_file($csv_file)) {
        $input = fopen($csv_file, 'a+');

        $counter = 1;
        while ($row = fgetcsv($input, 1024, ';')) {

            if($row[0] == null || $row[1] == null || $row[2] == null || $row[3] == null || $row[4] == null)
            {
                $errorNewTeacher = "Neužpildytas laukas " . $counter . " eilutėje";
                break;
            }
            else
            {
                $teacherName = $row[0];
                $teacherLastName = $row[1];
                $teacherCode = $row[2];
                $teacherRoom = $row[3];
                $teacherSubject = $row[4];

                if($teacherSubject == "" || !preg_match(Patterns::ONLY_NUMBERS, $teacherSubject) || !$subject->checkIfSubjectExists($teacherSubject))
                {
                    $errorNewTeacher = "Blogas mokomas dalykas " . $counter . " eilutėje";
                }
                else if(strlen($teacherName) < 1 || !preg_match(Patterns::ONLY_LETTERS, $teacherName))
                {
                    $errorNewTeacher = "Blogas mokytojo vardas " . $counter . " eilutėje";
                }
                else if(strlen($teacherLastName) < 1 || !preg_match(Patterns::ONLY_LETTERS, $teacherLastName))
                {
                    $errorNewTeacher = "Bloga mokytojo pavardė " . $counter . " eilutėje";
                }
                else if(strlen($teacherRoom) < 3 || !preg_match(Patterns::ROOM_NAME, $teacherRoom))
                {
                    $errorNewTeacher = "Klaidingas kabinetas " . $counter . " eilutėje";
                }
                else if($room->checkIfRoomExists($teacherRoom))
                {
                    $errorNewTeacher = "Kabinetas užimtas " . $counter . " eilutėje";
                }
                else if(!preg_match(Patterns::CITIZEN_CODE, $teacherCode))
                {
                    $errorNewTeacher = "Blogas asmens kodas " . $counter . " eilutėje";
                }
                else if($auth_user->checkIfCitizenCodeExists($teacherCode))
                {
                    $errorNewTeacher = "Toks asmens kodas jau yra " . $counter . " eilutėje";
                }
                else
                {
                    $auth_user->addNewTeacherToSystem($teacherName, $teacherLastName, $teacherCode, $teacherRoom, $teacherSubject);

                    $successNewTeacher = "Mokytojai sėkmingai pridėti iš failo";
                }
            }

            $counter++;
        }
    }
    else
    {
        $errorNewTeacher = "Blogas pasirinktas failas";
    }
}
else if (isset($_POST['submitNewStudentFile']))
{
    $csv_file =  $_FILES['csv_file']['tmp_name'];
    if (is_file($csv_file))
    {
        $input = fopen($csv_file, 'a+');

        $counter = 1;
        while ($row = fgetcsv($input, 1024, ';'))
        {
            if ($row[0] == null || $row[1] == null || $row[2] == null || $row[3] == null)
            {
                $errorNewTeacher = "Neužpildytas laukas " . $counter . " eilutėje";
                break;
            }
            else
            {
                $studentName = $row[0];
                $studenLastName =$row[1];
                $studentCode = $row[2];
                $studenClass = $row[3];

                if(strlen($studentName) < 1 || !preg_match(Patterns::ONLY_LETTERS, $studentName))
                {
                    $errorNew = "Blogas moksleivio vardas " . $counter . " eilutėje";
                }
                else if(strlen($studenLastName) < 1 || !preg_match(Patterns::ONLY_LETTERS, $studenLastName))
                {
                    $errorNew = "Bloga moksleivio pavardė " . $counter . " eilutėje";
                }
                else if($auth_user->checkIfCitizenCodeExists($studentCode) || !preg_match(Patterns::CITIZEN_CODE, $studentCode))
                {
                    $errorNew = "Blogas asmens kodas " . $counter . " eilutėje";
                }
                else if($studenClass == "" || !preg_match(Patterns::ONLY_NUMBERS, $studenClass))
                {
                    $errorNew = "Bloga klasė " . $counter . " eilutėje";
                }
                else if(!$schoolClass->checkIfClassExists($studenClass))
                {
                    $errorNew = "Klasė neegzistuoja " . $counter . " eilutėje";
                }
                else
                {
                    $auth_user->addNewStudentToSystem($studentName, $studenLastName, $studentCode, $studenClass);

                    $successNew = "Moksleiviai sėkmingai pridėti iš failo";
                }
            }

            $counter++;
        }
    }
    else
    {
        $errorNew = "Blogas pasirinktas failas";
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Vartotojai</title>
    <link rel="import" href="../includes/basicHeadInclude.html">
</head>
<body>
<?php include_once '../includes/adminNavbar.php'; ?>

<div class="container">

    <h3>Pridėti mokytoją</h3>
    <div class="row">
        <form method="post">
            <div class="form-group col-lg-2">
                <label for="newTeacherName">Vardas:</label>
                <input type="text" class="form-control" id="newTeacherName" name="newTeacherName">
            </div>
            <div class="form-group col-lg-2">
                <label for="newTeacherLastName">Pavardė:</label>
                <input type="text" class="form-control" id="newTeacherLastName" name="newTeacherLastName">
            </div>
            <div class="form-group col-lg-3">
                <label for="newTeacherCode">Asmens kodas:</label>
                <input type="text" class="form-control" id="newTeacherCode" name="newTeacherCode" maxlength="11" pattern="^\d{11}$" oninvalid="this.setCustomValidity('Neteisingai užpildytas asmens kodas')" oninput="setCustomValidity('')">
            </div>
            <div class="form-group col-lg-2">
                <label for="newTeacherRoom">Kabinetas:</label>
                <input type="text" class="form-control" id="newTeacherRoom" name="newTeacherRoom" maxlength="4" pattern="^\d{3}[a-z]{0,1}$" oninvalid="this.setCustomValidity('Neteisingai užpildytas kabinetas')" oninput="setCustomValidity('')">
            </div>
            <div class="form-group col-lg-3">
                <label for="subject">Dalykas:</label>
                <select class="form-control" id="subject" name="subject">
                    <?php
                    $allSubjects = $subject->getAllSubjects();
                    if($allSubjects != null && $allSubjects->rowCount() > 0)
                    {
                        $allSubjects = $allSubjects->fetchAll();
                        foreach ($allSubjects as $sub)
                        {
                            ?><option value="<?php echo $sub['id']; ?>"><?php echo $sub['name']; ?></option><?php
                        }
                    }
                    else
                    {
                        ?><option value="" disabled selected>Nėra dalykų</option><?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-xs-1">
                <input type="submit" class="btn btn-info" name="submitNewTeacher" value="Pridėti">
            </div>
        </form>

        <form method="post" enctype="multipart/form-data" name="importForm" id="importForm">
            <div class="form-group col-md-4">
            <input name="csv_file" type="file" accept=".csv" id="csv_file" class="file_input btn btn-info btn-file"/>
            </div>
            <div class="form-group col-lg-3">
            <input type="submit" class="btn btn-info" name="submitNewTeacherFile" value="Pridėti iš failo">
            </div>
        </form>

    </div>

    <?php
    if ( isset($errorNewTeacher) )
    {

        ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $errorNewTeacher ?>
        </div>
        <?php
    }
    else if ( isset($successNewTeacher) )
    {

        ?>
        <div class="alert alert-success" role="alert">
            <?php echo $successNewTeacher ?>
        </div>
        <?php
    }
    ?>

    <h3>Mokytojų sąrašas</h3>

    <table class="table table-hover">

        <thead>
        <tr>
            <th>#</th>
            <th>Vardas</th>
            <th>Pavardė</th>
            <th>Dalykas</th>
            <th>Kabinetas</th>
            <th>Asmens kodas</th>
            <th>Registracijos kodas</th>
            <th>Veiksmai</th>
        </tr>
        </thead>

        <tbody>
        <?php
        $teacherList = $auth_user->getAllTeachers();
        if($teacherList != null && $teacherList->rowCount() > 0)
        {
            $teacherList = $teacherList->fetchAll();
            $counter = 1;
            foreach ($teacherList as $s)
            {
                ?><tr><?php
                ?><td><?php echo $counter; ?></td><?php
                ?><td><?php echo $s['name']; ?></td><?php
                ?><td><?php echo $s['lastname']; ?></td><?php
                ?><td><?php echo $s['subjectname']; ?></td><?php
                ?><td><?php echo $s['roomname']; ?></td><?php
                ?><td><?php echo $s['citizencode']; ?></td><?php
                ?><td><?php echo $s['registrationcode']; ?></td><?php

                ?><td class="user-edit">
                <form method="post">
                    <input type="hidden" name="deleteTeacherId" value="<?php echo $s['userid']; ?>">
                    <input type="hidden" name="deleteTeacherTrueId" value="<?php echo $s['teacherid']; ?>">
                    <input type="submit" class="btn btn-link" name="submitDeleteTeacher" value="Trinti">
                    <a href="adminTeacherModification.php?modificationId=<?php echo $s['teacherid']?>" class="btn btn-link" role="button">Redaguoti</a>
                </form>
                </td><?php

                ?></tr><?php

                $counter++;
            }
        }
        ?>
        </tbody>
    </table>

    <?php
    if ( isset($successTeacherDelete) )
    {

        ?>
        <div class="alert alert-success" role="alert">
            <?php echo $successTeacherDelete ?>
        </div>
        <?php
    }
    ?>

    <h3>Pridėti moksleivį</h3>
    <div class="row">
        <form method="post">
            <div class="form-group col-lg-3">
                <label for="newStudentName">Vardas:</label>
                <input type="text" class="form-control" id="newStudentName" name="newStudentName">
            </div>
            <div class="form-group col-lg-3">
                <label for="newStudentLastName">Pavardė:</label>
                <input type="text" class="form-control" id="newStudentLastName" name="newStudentLastName">
            </div>
            <div class="form-group col-lg-3">
                <label for="newStudentCode">Asmens kodas:</label>
                <input type="text" class="form-control" id="newStudentCode" name="newStudentCode" maxlength="11" pattern="^\d{11}$" oninvalid="this.setCustomValidity('Neteisingai užpildytas asmens kodas')" oninput="setCustomValidity('')">
            </div>
            <div class="form-group col-lg-3">
                <label for="schoolClass">Klasė:</label>
                <select class="form-control" id="schoolClass" name="schoolClass">
                    <?php
                    $allClasses = $schoolClass->getAllClasses();
                    if($allClasses != null && $allClasses->rowCount() > 0)
                    {
                        $allClasses = $allClasses->fetchAll();
                        foreach ($allClasses as $class)
                        {
                            ?><option value="<?php echo $class['classid']; ?>"><?php echo $class['classname']; ?></option><?php
                        }
                    }
                    else
                    {
                        ?><option value="" disabled selected>Nėra klasių</option><?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-xs-1">
                <input type="submit" class="btn btn-info" name="submitNewStudent" value="Pridėti">
            </div>
        </form>

        <form method="post" enctype="multipart/form-data" name="importForm" id="importForm">
            <div class="form-group col-md-4">
                <input name="csv_file" type="file" accept=".csv" id="csv_file" class="file_input btn btn-info btn-file"/>
            </div>
            <div class="form-group col-lg-3">
                <input type="submit" class="btn btn-info" name="submitNewStudentFile" value="Pridėti iš failo">
            </div>
        </form>

    </div>

    <?php
    if ( isset($errorNew) )
    {

        ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $errorNew ?>
        </div>
        <?php
    }
    else if ( isset($successNew) )
    {

        ?>
        <div class="alert alert-success" role="alert">
            <?php echo $successNew ?>
        </div>
        <?php
    }
    ?>

    <h3>Moksleivių sąrašas</h3>

    <table class="table table-hover">

        <thead>
        <tr>
            <th>#</th>
            <th>Vardas</th>
            <th>Pavardė</th>
            <th>Klasė</th>
            <th>Asmens kodas</th>
            <th>Registracijos kodas</th>
            <th>Veiksmai</th>
        </tr>
        </thead>

        <tbody>
        <?php
        $studentList = $auth_user->getAllStudents();
        if($studentList != null && $studentList->rowCount() > 0)
        {
            $studentList = $studentList->fetchAll();
            $counter = 1;
            foreach ($studentList as $s)
            {
                ?><tr><?php
                ?><td><?php echo $counter; ?></td><?php
                ?><td><?php echo $s['name']; ?></td><?php
                ?><td><?php echo $s['lastname']; ?></td><?php
                ?><td><?php echo $s['classname']; ?></td><?php
                ?><td><?php echo $s['citizencode']; ?></td><?php
                ?><td><?php echo $s['registrationcode']; ?></td><?php

                ?><td class="user-edit">
                <form method="post">
                    <input type="hidden" name="deleteId" value="<?php echo $s['userid']; ?>">
                    <input type="submit" class="btn btn-link" name="submitDeleteStudent" value="Trinti">
                    <a href="adminStudentModification.php?modificationId=<?php echo $s['studentid']?>" class="btn btn-link" role="button">Redaguoti</a>
                </form>
                </td><?php

                ?></tr><?php

                $counter++;
            }
        }
        ?>
        </tbody>
    </table>

</div>

</body>
</html>
