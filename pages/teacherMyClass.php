<?php
    require_once("../utils/teacherSession.php");

    require_once("../libraries/User.php");
    $auth_user = new User();

    $user_id = $_SESSION['user_session'];

    $stmt = $auth_user->runQuery("SELECT * FROM users WHERE id=:user_id");
    $stmt->execute(array(":user_id"=>$user_id));

    $userRow=$stmt->fetch(PDO::FETCH_ASSOC);

    require_once("../libraries/Teacher.php");
    $teacher = new Teacher();
    $teacherInfo = $teacher->getTeacher($user_id);

    require_once("../libraries/SchoolClass.php");
    $schoolClass = new SchoolClass();
    $classInfo = $schoolClass->getClassForTeacher($teacherInfo['id']);
?>

<!DOCTYPE html>
<html>
<head>
    <title>Atostogos</title>
    <link rel="import" href="../includes/basicHeadInclude.html">
</head>
<body>
<?php include_once '../includes/teacherNavbar.php'; ?>

<div class="container">
    <div>
        <h2>Auklėtinių sąrašas</h2>

        <table class="table table-hover">

            <thead>
            <tr>
                <th>#</th>
                <th>Vardas</th>
                <th>Pavardė</th>
                <th>Klasė</th>
            </tr>
            </thead>

            <tbody>
            <?php
            if($classInfo != null) {
                $classInfo = $classInfo->fetchAll();
                $counter = 1;
                foreach ($classInfo as $student)
                {
                    ?><tr><?php
                    ?><td><?php echo $counter; ?></td><?php
                    ?><td><?php echo $student['name']; ?></td><?php
                    ?><td><?php echo $student['lastname']; ?></td><?php
                    ?><td><?php echo $student['classname']; ?></td><?php
                    ?></tr><?php

                    $counter++;
                }
            }
            ?>
            </tbody>

        </table>
    </div>
</div>

</body>
</html>
