<?php
    require_once("../utils/studentSession.php");

    require_once("../libraries/User.php");
    $auth_user = new User();

    $user_id = $_SESSION['user_session'];

    $stmt = $auth_user->runQuery("SELECT * FROM users WHERE id=:user_id");
    $stmt->execute(array(":user_id"=>$user_id));

    $userRow=$stmt->fetch(PDO::FETCH_ASSOC);

    require_once("../libraries/Student.php");
    $student = new Student();

    $studentInfo = $student->getStudent($user_id);

    require_once ("../libraries/Schedule.php");
    $schedule = new Schedule();

    $allScheduleTime = $schedule->getAllScheduleTimes();
    $scheduleDays = $schedule->getAllScheduleDays();
    $scheduleDays = $scheduleDays->fetchAll();
?>

<!DOCTYPE html>
<html>
<head>
    <title>Tvarkaraštis</title>
    <link rel="import" href="../includes/basicHeadInclude.html">
</head>
<body>
<?php include_once '../includes/studentNavbar.php'; ?>

<div class="container">
    <div>
        <h2>Tvarkaraštis</h2>

        <table class="table table-hover table-bordered">

            <thead>
            <tr>
                <th>Laikas</th>
                <?php
                    foreach ($scheduleDays as $scheduleDay)
                    {
                        ?><th><?php echo $scheduleDay['name']; ?></th><?php
                    }
                ?>
            </tr>
            </thead>

            <tbody>
            <?php

                while($scheduleTime = $allScheduleTime->fetch(PDO::FETCH_ASSOC))
                {
                    ?><tr><?php
                    ?><td><?php echo $scheduleTime['fromTime'] ."-". $scheduleTime['toTime']; ?></td><?php
                    foreach ($scheduleDays as $scheduleDay)
                    {
                        $subject = $schedule->getSubjectForClassTimeAndDay($studentInfo['fk_classid'], $scheduleTime['id'], $scheduleDay['id']);
                        ?><td><?php if($subject['subjectname'] != ""){ ?><button type="button" class="btn btn-link" data-toggle="modal" data-target="#scheduleModal" data-classinfo="<p><b>Diena:</b> <?php echo $subject['dayname']; ?></p><p><b>Pamoka:</b> <?php echo $subject['subjectname']; ?></p><p><b>Kabinetas:</b> <?php echo $subject['roomname']; ?></p><p><b>Mokytojas:</b> <?php echo $subject['teachername'] . " " . $subject['teacherlastname']; ?></p>"><?php echo $subject['subjectname']; ?></button> <?php } else echo "-"; ?></td><?php
                    }
                    ?></tr><?php
                }
            ?>
            </tbody>

        </table>

        <div class="modal fade bs-modal-sm" id="scheduleModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
            <div class="modal-dialog modal-sm" role="document">

                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Informacija</h4>
                        <div class="modal-body">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Uždaryti</button>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $('#scheduleModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var dayInfo = button.data('classinfo');
                var modal = $(this);
                modal.find('.modal-body').html(dayInfo);
            })
        </script>
</div>

</body>
</html>
