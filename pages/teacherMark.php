<?php
require_once("../utils/teacherSession.php");

require_once("../libraries/User.php");
$auth_user = new User();

$user_id = $_SESSION['user_session'];

$stmt = $auth_user->runQuery("SELECT * FROM users WHERE id=:user_id");
$stmt->execute(array(":user_id"=>$user_id));

$userRow=$stmt->fetch(PDO::FETCH_ASSOC);

require_once("../libraries/Teacher.php");
$teacher = new Teacher();

$teacherInfo = $teacher->getTeacher($user_id);

require_once("../libraries/Schedule.php");
$schedule = new Schedule();
$teachersClasses = $schedule->getClassListForTeacher($teacherInfo['id']);
if($teachersClasses != null)
{
    $teachersClasses = $teachersClasses->fetchAll();
}

require_once("../libraries/SchoolClass.php");
$schoolClass = new SchoolClass();

require_once("../libraries/Marks.php");
$marks = new Marks();

require_once("../libraries/Semester.php");
$semester = new Semester();
$allSemesters = $semester->getAllSemesters();
if($allSemesters != null)
{
    $allSemesters = $allSemesters->fetchAll();
}

require_once("../libraries/SemesterMark.php");
$semesterMark = new SemesterMark();

setlocale(LC_ALL, 'lt-LT');
$currentMonth = date("m");
$currentYear = date("Y");

if(isset($_POST['submit']))
{
    if(!isset($_POST['schoolClass']))
    {
        $errorNew = "Nėra klasės";
    }
    else if(!isset($_POST['schoolClassStudent']))
    {
        $errorNew = "Nėra moksleivio";
    }
    else
    {
        $newMarkStudent = $_POST['schoolClassStudent'];
        $newMark = $_POST['mark'];
        $markSummary = $_POST['summary'];
        $markDate = $_POST['markDate'];

        if (strlen($markDate) < 1) {
            $errorNew = "Data turi būti nurodyta";
        }
        else if(strlen($newMark) < 1)
        {
            $errorNew = "Pažymys turi būti nurodytas";
        }
        else if($marks->checkIfStudentHasMarkForDateAndSubject($newMarkStudent, $markDate, $teacherInfo['fk_subjectid']))
        {
            $errorNew = "Pasirinkta data jau yra įrašytas pažymys";
        }
        else {
            $marks->addMarkForStudent($newMarkStudent, $teacherInfo['id'], $markSummary, $markDate, $newMark, $teacherInfo['fk_subjectid']);

            $successNew = "Pažymys pridėtas";
        }
    }
}
else if(isset($_POST['submitShow']))
{
    if(!isset($_POST['schoolClassFind']))
    {
        $errorFind = "Nėra klasės";
    }
    else if(!isset($_POST['schoolClassStudentFind']))
    {
        $errorFind = "Nėra moksleivio";
    }
    else {
        $markClass = $_POST['schoolClassFind'];
        $markFromDate = $_POST['findFromDate'];
        $markToDate = $_POST['findToDate'];
        $markStudent = $_POST['schoolClassStudentFind'];

        if (strlen($markFromDate) < 1) {
            $errorFind = "Data turi būti nurodyta";
        } else if (strlen($markToDate) < 1) {
            $errorFind = "Data turi būti nurodyta";
        } else {
            $marksInRange = $marks->getMarksForStudentTeacherAndDateRange($markStudent, $teacherInfo['id'], $markFromDate, $markToDate);
            if ($marksInRange != null && $marksInRange->rowCount() > 0) {
                $marksInRange = $marksInRange->fetchAll();
            }
        }
    }
}

if(isset($_POST['submitDelete']))
{
    $marks->deleteMark($_POST['deleteId']);
    $successDel = "Pažymys ištrintas";
}

if(isset($_POST['submitSemesterShow']))
{
    if(!isset($_POST['schoolClassSemesterFind']))
    {
        $errorFind = "Nėra klasės";
    }
    else if(!isset($_POST['semesterFind']))
    {
        $errorFind = "Nėra semestro";
    }
    else
    {
        $semesterClass = $_POST['schoolClassSemesterFind'];
        $semesterFind = $_POST['semesterFind'];

        $allStudentsInClass = $schoolClass->getClassStudentsForId($semesterClass);
        if ($allStudentsInClass != null && $allStudentsInClass->rowCount() > 0)
        {
            $allStudentsInClass = $allStudentsInClass->fetchAll();
        }
    }
}

if(isset($_POST['submitNewSemesterMark']))
{
    $studentIdForSemesterMark = $_POST['studentIdForSemesterMark'];
    $yearForSemesterMark = $_POST['yearForSemesterMark'];
    $semesterIdForSemesterMark = $_POST['semesterIdForSemesterMark'];
    $markForSemester = $marks->getRoundedAverageForStudentSubjectYearAndSemester($teacherInfo['fk_subjectid'], $studentIdForSemesterMark, $yearForSemesterMark, $semesterIdForSemesterMark);

    $semesterMark->addSemesterMarkForStudentYearTeacherSubjectAndSemester($markForSemester['average'], $studentIdForSemesterMark, $yearForSemesterMark, $teacherInfo['id'], $teacherInfo['fk_subjectid'], $semesterIdForSemesterMark);

    $successSemester = "Pažymys išvestas";
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Pažymiai</title>
    <link rel="import" href="../includes/basicHeadInclude.html">
</head>
<body>
<?php include_once '../includes/teacherNavbar.php'; ?>

<div class="container">
    <h3>Naujas pažymis</h3>

    <div class="row">
        <form method="post">
            <div class="form-group col-lg-2">
                <label for="schoolClass">Klasė:</label>
                <select class="form-control" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);" id="schoolClass" name="schoolClass">
                    <option value="">Pasirinkti...</option>
                    <?php
                    foreach ($teachersClasses as $class)
                    {
                        ?><option value="../pages/teacherMark.php?classId=<?php echo $class['fk_classid']; ?>" <?php if(isset($_GET['classId']) && $_GET['classId'] == $class['fk_classid']) echo " selected"; ?>><?php echo $class['name']; ?></option><?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-lg-3">
                <label for="schoolClassStudent">Mokinys:</label>
                <select class="form-control" id="schoolClassStudent" name="schoolClassStudent">
                    <?php
                    if(isset($_GET['classId'])) {
                        $studentsInClass = $schoolClass->getClassStudentsForId($_GET['classId']);
                        if($studentsInClass != null && $studentsInClass->rowCount() > 0) {
                            $studentsInClass = $studentsInClass->fetchAll();
                            foreach ($studentsInClass as $s) {
                                ?><option value="<?php echo $s['studentid']; ?>"><?php echo $s['name'] . " " . $s['lastname']; ?></option><?php
                            }
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-lg-2">
                <label for="mark">Pažymis:</label>
                <input type="text" class="form-control" id="mark" name="mark" pattern="^[1-9]|10$" oninvalid="this.setCustomValidity('Neteisingai užpildytas laukas')" oninput="setCustomValidity('')">
            </div>
            <div class="form-group col-lg-3">
                <label for="summary">Aprašas:</label>
                <input type="text" class="form-control" id="summary" name="summary">
            </div>
            <div class="form-group col-lg-2">
                <label for="markDate">Data:</label>
                <input type="date" class="form-control" id="markDate" name="markDate">
            </div>
            <div class="form-group col-lg-4">
                <input type="submit" class="btn btn-info" name="submit" value="Įvesti">
            </div>
        </form>
    </div>

    <?php
    if ( isset($errorNew) )
    {

        ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $errorNew ?>
        </div>
        <?php
    }
    else if ( isset($successNew) )
    {

        ?>
        <div class="alert alert-success" role="alert">
            <?php echo $successNew ?>
        </div>
        <?php
    }
    ?>

    <h3>Rodyti pažymius</h3>

    <div class="row">
        <form method="post">
            <div class="form-group col-lg-3">
                <label for="schoolClassFind">Klasė:</label>
                <select class="form-control" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);" id="schoolClassFind" name="schoolClassFind">
                    <option value="">Pasirinkti...</option>
                    <?php
                    foreach ($teachersClasses as $class)
                    {
                        ?><option value="../pages/teacherMark.php?classIdFind=<?php echo $class['fk_classid']; ?>" <?php if(isset($_GET['classIdFind']) && $_GET['classIdFind'] == $class['fk_classid']) echo " selected"; ?>><?php echo $class['name']; ?></option><?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-lg-3">
                <label for="schoolClassStudentFind">Mokinys:</label>
                <select class="form-control" id="schoolClassStudentFind" name="schoolClassStudentFind">
                    <?php
                    if(isset($_GET['classIdFind'])) {
                        $studentsInClass = $schoolClass->getClassStudentsForId($_GET['classIdFind']);
                        if($studentsInClass != null && $studentsInClass->rowCount() > 0) {
                            $studentsInClass = $studentsInClass->fetchAll();
                            foreach ($studentsInClass as $s) {
                                ?><option value="<?php echo $s['studentid']; ?>"><?php echo $s['name'] . " " . $s['lastname']; ?></option><?php
                            }
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-lg-3">
                <label for="findFromDate">Nuo:</label>
                <input type="date" class="form-control" id="findFromDate" name="findFromDate">
            </div>
            <div class="form-group col-lg-3">
                <label for="findToDate">Nuo:</label>
                <input type="date" class="form-control" id="findToDate" name="findToDate">
            </div>
            <div class="form-group col-lg-4">
                <input type="submit" class="btn btn-info" name="submitShow" value="Rodyti">
            </div>
        </form>
    </div>

    <?php
    if ( isset($errorFind) )
    {

        ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $errorFind ?>
        </div>
        <?php
    }
    ?>

    <table class="table table-hover">

        <thead>
        <tr>
            <th>#</th>
            <th>Pažymis</th>
            <th>Aprašas</th>
            <th>Data</th>
            <th>Veiksmai</th>
        </tr>
        </thead>

        <tbody>
        <?php
        if(isset($marksInRange))
        {
            $counter = 1;
            foreach ($marksInRange as $hm)
            {
                ?><tr><?php
                ?><td><?php echo $counter; ?></td><?php
                ?><td><?php echo $hm['value']; ?></td><?php
                ?><td><?php echo $hm['summary']; ?></td><?php
                ?><td><?php echo $hm['date']; ?></td><?php

                ?><td class="mark-edit">
                <form method="post">
                    <input type="hidden" name="deleteId" value="<?php echo $hm['id']; ?>">
                    <input type="submit" class="btn btn-link" name="submitDelete" value="Trinti">
                    <a href="teacherMarkModification.php?modificationId=<?php echo $hm['id']?>" class="btn btn-link" role="button">Redaguoti</a>
                </form>
                </td><?php

                ?></tr><?php

                $counter++;
            }
        }
        ?>
        </tbody>
    </table>

    <?php
    if ( isset($successDel) )
    {

        ?>
        <div class="alert alert-success" role="alert">
            <?php echo $successDel ?>
        </div>
        <?php
    }
    ?>

    <h3>Rodyti semestro pažymius</h3>

    <div class="row">
        <form method="post">
            <div class="form-group col-lg-3">
                <label for="schoolClassSemesterFind">Klasė:</label>
                <select class="form-control" id="schoolClassSemesterFind" name="schoolClassSemesterFind">
                    <?php
                    foreach ($teachersClasses as $class)
                    {
                        ?><option value="<?php echo $class['fk_classid']; ?>" <?php if(isset($_POST['schoolClassSemesterFind']) && $_POST['schoolClassSemesterFind'] == $class['fk_classid']) echo " selected"; ?>><?php echo $class['name']; ?></option><?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-lg-3">
                <label for="semesterFind">Semestras:</label>
                <select class="form-control" id="semesterFind" name="semesterFind">
                    <?php
                    foreach ($allSemesters as $s)
                    {
                        ?><option value="<?php echo $s['id']; ?>" <?php if(isset($_POST['semesterFind']) && $_POST['semesterFind'] == $s['id']) echo " selected"; ?>><?php echo $s['name']; ?></option><?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-lg-12">
                <input type="submit" class="btn btn-info" name="submitSemesterShow" value="Rodyti">
            </div>
        </form>
    </div>

    <table class="table table-hover">

        <thead>
        <tr>
            <th>#</th>
            <th>Vardas</th>
            <th>Pavardė</th>
            <th>Vidurkis</th>
            <th>Galutinis</th>
        </tr>
        </thead>

        <tbody>
        <?php
        if(isset($allStudentsInClass))
        {
            $counter = 1;
            foreach ($allStudentsInClass as $hm)
            {
                ?><tr><?php
                ?><td><?php echo $counter; ?></td><?php
                ?><td><?php echo $hm['name']; ?></td><?php
                ?><td><?php echo $hm['lastname']; ?></td><?php
                if($currentMonth >= 1 && $currentMonth <= 5 && $_POST['semesterFind'] == 2)
                {
                    $yearToSelect = date("Y");
                }
                else if($currentMonth >= 1 && $currentMonth <= 5 && $_POST['semesterFind'] == 1)
                {
                    $yearToSelect = date("Y") - 1;
                }
                else if($currentMonth >= 9 && $currentMonth <= 12 && $_POST['semesterFind'] == 1)
                {
                    $yearToSelect = date("Y");
                }
                else
                {
                    $yearToSelect = date("Y") + 1;
                }
                $average = $marks->getAverageForStudentSubjectYearAndSemester($teacherInfo['fk_subjectid'], $hm['studentid'], $yearToSelect, $_POST['semesterFind']);
                ?><td><?php if($average['average'] == "") echo "-"; else echo $average['average']; ?></td><?php

                $currentSemesterMark = $semesterMark->getSemesterMarkForStudentYearSubjectAndSemester($hm['studentid'], $yearToSelect, $teacherInfo['fk_subjectid'], $_POST['semesterFind']);
                if($currentSemesterMark == null)
                {
                    ?><td class="semester-edit">
                    <form method="post">
                        <input type="hidden" name="studentIdForSemesterMark" value="<?php echo $hm['studentid']; ?>">
                        <input type="hidden" name="yearForSemesterMark" value="<?php echo $yearToSelect; ?>">
                        <input type="hidden" name="semesterIdForSemesterMark" value="<?php echo $_POST['semesterFind']; ?>">
                        <input type="submit" class="btn btn-link" name="submitNewSemesterMark" value="Patvirtinti" <?php if($average['average'] == "") echo "disabled";?>>
                    </form>
                    </td><?php
                }
                else
                {
                    ?><td><?php echo $currentSemesterMark['value']; ?></td><?php
                }
                ?></tr><?php

                $counter++;
            }
        }
        ?>
        </tbody>
    </table>

    <?php if ( isset($successSemester) )
    {
        ?>
        <div class="alert alert-success" role="alert">
            <?php echo $successSemester ?>
        </div>
        <?php
    }
    ?>

</div>

</body>
</html>
