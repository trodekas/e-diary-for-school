<?php
require_once("../utils/adminSession.php");

require_once("../libraries/User.php");
$auth_user = new User();

$user_id = $_SESSION['user_session'];

$stmt = $auth_user->runQuery("SELECT * FROM users WHERE id=:user_id");
$stmt->execute(array(":user_id"=>$user_id));

$userRow=$stmt->fetch(PDO::FETCH_ASSOC);

require_once("../libraries/SchoolClass.php");
$schoolClass = new SchoolClass();

require_once("../libraries/Schedule.php");
$schedule = new Schedule();

require_once("../libraries/Subject.php");
$subject = new Subject();

require_once("../libraries/Teacher.php");
$teacher = new Teacher();

$currentScheduleClass = $schedule->getScheduleClass($_GET['modificationId']);

if(isset($_POST['submitUpdateScheduleClass']))
{
    if(!isset($_POST['newScheduleDay']))
    {
        $error = "Nėra dienos";
    }
    else if(!isset($_POST['newScheduleTime']))
    {
        $error = "Nėra valandos";
    }
    else if($schedule->checkIfClassExistsForTeacherDayAndTime($currentScheduleClass['fk_teacherid'], $_POST['newScheduleDay'], $_POST['newScheduleTime']))
    {
        $error = "Tuo laiku mokytojas turi užsiėmimą";
    }
    else if($schedule->checkIfClassExistsForClassDayAndTime($currentScheduleClass['fk_classid'], $_POST['newScheduleDay'], $_POST['newScheduleTime']))
    {
        $error = "Tuo laiku klasė turi užsiėmimą";
    }
    else
    {
        $newScheduleClass = $_GET['modificationId'];
        $newScheduleTime = $_POST['newScheduleTime'];
        $newScheduleDay = $_POST['newScheduleDay'];

        $schedule->updateScheduleClass($newScheduleClass, $newScheduleTime, $newScheduleDay);

        $success = "Tvarkaraščio pamoka sėkmingai atnaujinta";
        $currentScheduleClass = $schedule->getScheduleClass($_GET['modificationId']);
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Tvarkaraštis</title>
    <link rel="import" href="../includes/basicHeadInclude.html">
</head>
<body>
<?php include_once '../includes/adminNavbar.php'; ?>

<div class="container">
    <h3>Redaguoti tvarkaraščio pamoką</h3>
    <div class="row">
        <form method="post">
            <div class="form-group col-lg-3">
                <label for="teacherName">Mokytojas:</label>
                <input type="text" class="form-control" id="teacherName" name="teacherName" readonly value="<?php $cT = $teacher->getTeacherForTeacherId($currentScheduleClass['fk_teacherid']); echo $cT['name'] . " " . $cT['lastname'];?>">
            </div>
            <div class="form-group col-lg-3">
                <label for="teacherSubject">Mokytojas:</label>
                <input type="text" class="form-control" id="teacherSubject" name="teacherSubject" readonly value="<?php $cS = $subject->getSubject($currentScheduleClass['fk_subjectid']); echo $cS['name'];?>">
            </div>
            <div class="form-group col-lg-3">
                <label for="newScheduleDay">Diena:</label>
                <select class="form-control" id="newScheduleDay" name="newScheduleDay">
                    <?php
                    $allDays = $schedule->getAllScheduleDays();

                    if ($allDays != null && $allDays->rowCount() > 0)
                    {
                        $allDays = $allDays->fetchAll();

                        foreach ($allDays as $sub)
                        {
                            ?><option value="<?php echo $sub['id']; ?>" <?php if($currentScheduleClass['fk_dayid'] == $sub['id']) echo " selected"; ?>><?php echo $sub['name']; ?></option><?php
                        }
                    }
                    else
                    {
                        ?><option value="" disabled selected>Nėra dienų</option><?php
                    }

                    ?>
                </select>
            </div>
            <div class="form-group col-lg-3">
                <label for="newScheduleTime">Laikas:</label>
                <select class="form-control" id="newScheduleTime" name="newScheduleTime">
                    <?php
                    $allTimes = $schedule->getAllScheduleTimes();

                    if ($allTimes != null && $allTimes->rowCount() > 0)
                    {
                        $allTimes = $allTimes->fetchAll();

                        foreach ($allTimes as $sub)
                        {
                            ?><option value="<?php echo $sub['id']; ?>" <?php if($currentScheduleClass['fk_timeid'] == $sub['id']) echo " selected"; ?>><?php echo $sub['fromTime'] . "-" . $sub['toTime']; ?></option><?php
                        }
                    }
                    else
                    {
                        ?><option value="" disabled selected>Nėra laikų</option><?php
                    }

                    ?>
                </select>
            </div>
            <div class="form-group col-lg-12">
                <input type="submit" class="btn btn-info" name="submitUpdateScheduleClass" value="Atnaujinti">
            </div>
        </form>
    </div>

    <?php
    if ( isset($error) )
    {

        ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $error ?>
        </div>
        <?php
    }
    else if ( isset($success) )
    {

        ?>
        <div class="alert alert-success" role="alert">
            <?php echo $success ?>
        </div>
        <?php
    }
    ?>
</div>

</body>
</html>
