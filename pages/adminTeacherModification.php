<?php
require_once("../utils/adminSession.php");

require_once("../libraries/User.php");
$auth_user = new User();

$user_id = $_SESSION['user_session'];

$stmt = $auth_user->runQuery("SELECT * FROM users WHERE id=:user_id");
$stmt->execute(array(":user_id"=>$user_id));

$userRow=$stmt->fetch(PDO::FETCH_ASSOC);

require_once("../libraries/Teacher.php");
$teacher = new Teacher();
$currentTeacher = $teacher->getTeacherForTeacherId($_GET['modificationId']);

require_once("../libraries/Room.php");
$room = new Room();

require_once("../libraries/Subject.php");
$subject = new Subject();

if(isset($_POST['submit']))
{
    $teacherName = $_POST['teacherName'];
    $teacherLastName = $_POST['teacherLastName'];
    $teacherCode = $_POST['teacherCode'];
    $teacherRoom = $_POST['teacherRoom'];
    $teacherSubjectId = $_POST['teacherSubject'];

    if(strlen($teacherName) < 1)
    {
        $errorNew = "Neįvestas mokytojo vardas";
    }
    else if(strlen($teacherLastName) < 1)
    {
        $errorNew = "Neįvesta mokytojo pavardė";
    }
    else if(strlen($teacherCode) < 11)
    {
        $errorNew = "Blogai suvestas asmens kodas";
    }
    else if(strlen($teacherRoom) < 1)
    {
        $errorNew = "Nesuvestas kabinetas";
    }
    else if($teacherSubjectId == "")
    {
        $errorNew = "Nėra dalyko";
    }
    else if($auth_user->checkIfCitizenCodeExistsIgnoreTeacherWithId($teacherCode, $_GET['modificationId']))
    {
        $errorNew = "Toks asmens kodas jau egzistuoja";
    }
    else if($room->checkIfRoomExistsIgnoreTeacherId($teacherRoom, $_GET['modificationId']))
    {
        $errorNew = "Kabinetas jau priskirtas kitam mokytojui";
    }
    else
    {
        $teacher->updateTeacher($_GET['modificationId'], $teacherName, $teacherLastName, $teacherCode, $teacherSubjectId, $teacherRoom);
        $successNew = "Mokytojas sėkmingai modifikuotas";
        $currentTeacher = $teacher->getTeacherForTeacherId($_GET['modificationId']);
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Mokytojo modifikavimas</title>
    <link rel="import" href="../includes/basicHeadInclude.html">
</head>
<body>
<?php include_once '../includes/adminNavbar.php'; ?>

<div class="container">
    <h3>Redaguoti mokytoją</h3>

    <div class="row">
        <form method="post">
            <div class="form-group col-lg-2">
                <label for="teacherName">Vardas:</label>
                <input type="text" class="form-control" id="teacherName" name="teacherName" value="<?php echo $currentTeacher['name']?>" minlength="1" pattern="^[a-zą-žA-ZĄ-Ž]+$" oninvalid="this.setCustomValidity('Neteisingai užpildytas vardas')" oninput="setCustomValidity('')">
            </div>
            <div class="form-group col-lg-2">
                <label for="teacherLastName">Pavardė:</label>
                <input type="text" class="form-control" id="teacherLastName" name="teacherLastName" value="<?php echo $currentTeacher['lastname']?>" minlength="1" pattern="^[a-zą-žA-ZĄ-Ž]+$" oninvalid="this.setCustomValidity('Neteisingai užpildyta pavardė')" oninput="setCustomValidity('')">
            </div>
            <div class="form-group col-lg-3">
                <label for="teacherCode">Asmens kodas:</label>
                <input type="text" class="form-control" id="teacherCode" name="teacherCode" value="<?php echo $currentTeacher['citizencode']?>" minlength="11" maxlength="11" pattern="^\d{11}$" oninvalid="this.setCustomValidity('Neteisingai užpildytas asmens kodas')" oninput="setCustomValidity('')">
            </div>
            <div class="form-group col-lg-3">
                <label for="teacherRoom">Kabinetas:</label>
                <input type="text" class="form-control" id="teacherRoom" name="teacherRoom" value="<?php $currentRoom = $room->getRoom($currentTeacher['fk_roomid']); echo $currentRoom['name']?>"  maxlength="4" pattern="^\d{3}[a-z]{0,1}$" oninvalid="this.setCustomValidity('Neteisingai užpildytas kabinetas')" oninput="setCustomValidity('')">
            </div>
            <div class="form-group col-lg-2">
                <label for="teacherSubject">Dalykas:</label>
                <select class="form-control" id="teacherSubject" name="teacherSubject">
                    <?php
                    $allSubjects = $subject->getAllSubjects();
                    if($allSubjects != null && $allSubjects->rowCount() > 0)
                    {
                        $allSubjects = $allSubjects->fetchAll();
                        foreach ($allSubjects as $sub)
                        {
                            ?><option <?php if($sub['id'] == $currentTeacher['fk_subjectid']) echo "selected"; ?> value="<?php echo $sub['id']; ?>"><?php echo $sub['name']; ?></option><?php
                        }
                    }
                    else
                    {
                        ?><option value="" disabled selected>Nėra dalykų</option><?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-lg-12">
                <input type="submit" class="btn btn-info" name="submit" value="Išsaugoti">
            </div>

        </form>
    </div>

    <?php
    if ( isset($errorNew) )
    {

        ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $errorNew ?>
        </div>
        <?php
    }
    else if ( isset($successNew) )
    {

        ?>
        <div class="alert alert-success" role="alert">
            <?php echo $successNew ?>
        </div>
        <?php
    }
    ?>

</div>
</body>
</html>
