<?php
    require_once("../utils/adminSession.php");

    require_once("../libraries/User.php");
    $auth_user = new User();

    $user_id = $_SESSION['user_session'];

    $stmt = $auth_user->runQuery("SELECT * FROM users WHERE id=:user_id");
    $stmt->execute(array(":user_id"=>$user_id));

    $userRow=$stmt->fetch(PDO::FETCH_ASSOC);

    require_once("../libraries/Teacher.php");
    $teacher = new Teacher();

    require_once("../libraries/SchoolClass.php");
    $schoolClass = new SchoolClass();

    if(isset($_POST['submit']))
    {
        if(!isset($_POST['schoolClassLeader']))
        {
            $errorNew = "Nenurodytas mokytojas";
        }
        else
        {
            $teacherId = $_POST['schoolClassLeader'];
            $className = $_POST['className'];

            if(strlen($className) < 1)
            {
                $errorNew = "Nurodykite klasės pavadinimą";
            }
            else if($schoolClass->checkIfClassNameExist($className))
            {
                $errorNew = "Tokia klasė jau egzistuoja";
            }
            else
            {
                $schoolClass->addNewClass($className, $teacherId);

                $successNew = "Nauja klasė pridėta";
            }
        }
    }
    else if(isset($_POST['submitDelete']))
    {
        $schoolClass->deleteClass($_POST['deleteId']);

        $successDelete = "Klasė sėkmingai pašalinta";
    }
?>

<!DOCTYPE html>
<html>
<head>
    <title>Klasės</title>
    <link rel="import" href="../includes/basicHeadInclude.html">
</head>
<body>
<?php include_once '../includes/adminNavbar.php'; ?>

<div class="container">
    <h3>Nauja klasė</h3>

    <div class="row">
        <form method="post">
            <div class="form-group col-lg-3">
                <label for="schoolClassLeader">Vadovas:</label>
                <select class="form-control" id="schoolClassLeader" name="schoolClassLeader">
                    <?php
                    $teachersWithoutClass = $teacher->getAllTeachersWithoutClass();
                    if($teachersWithoutClass != null && $teachersWithoutClass->rowCount() > 0) {
                        $teachersWithoutClass = $teachersWithoutClass->fetchAll();
                        foreach ($teachersWithoutClass as $t) {
                            ?><option value="<?php echo $t['teacherid']; ?>"><?php echo $t['name'] . " " . $t['lastname']; ?></option><?php
                        }
                    }
                    else
                    {
                        ?><option value="" disabled selected>Nėra laisvų mokytojų</option><?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-lg-3">
                <label for="className">Klasės pavadinimas:</label>
                <input type="text" class="form-control" id="className" name="className" oninvalid="this.setCustomValidity('Netinkamas klasės pavadinimo formatavimas')" oninput="setCustomValidity('')" maxlength="3" pattern="^[0-9]{1,2}[a-z]$">
            </div>
            <div class="form-group col-lg-12">
                <input type="submit" class="btn btn-info" name="submit" value="Pridėti">
            </div>
        </form>
    </div>

    <?php
    if ( isset($errorNew) )
    {

        ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $errorNew ?>
        </div>
        <?php
    }
    else if ( isset($successNew) )
    {

        ?>
        <div class="alert alert-success" role="alert">
            <?php echo $successNew ?>
        </div>
        <?php
    }
    ?>

    <h3>Klasių sąrašas</h3>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>Klasė</th>
            <th>Vadovas</th>
            <th>Mokinių skaičius</th>
            <th>Veiksmai</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $allSchoolClasses = $schoolClass->getAllClasses();
        if($allSchoolClasses != null && $allSchoolClasses->rowCount() > 0)
        {
            $allSchoolClasses = $allSchoolClasses->fetchAll();
            $counter = 1;
            foreach ($allSchoolClasses as $c)
            {
                ?><tr><?php
                ?><td><?php echo $counter; ?></td><?php
                ?><td><?php echo $c['classname']; ?></td><?php
                ?><td><?php echo $c['teachername'] . " " . $c['teacherlastname']; ?></td><?php
                ?><td><?php $studentCount = $schoolClass->getStudentCountInClass($c['classid']); echo $studentCount['studentCount']; ?></td><?php

                ?><td class="class-edit">
                <form method="post">
                    <input type="hidden" name="deleteId" value="<?php echo $c['classid']; ?>">
                    <input type="submit" class="btn btn-link" name="submitDelete" value="Trinti" <?php if(intval($studentCount['studentCount']) != 0) echo "disabled"; ?>>
                    <a href="adminClassModification.php?modificationId=<?php echo $c['classid']?>" class="btn btn-link" role="button">Redaguoti</a>
                </form>
                </td><?php

                ?></tr><?php
                $counter++;
            }
        }
        ?>
        </tbody>
    </table>

    <?php
    if ( isset($successDelete) )
    {

    ?>
    <div class="alert alert-success" role="alert">
        <?php echo $successDelete ?>
    </div>
    <?php
    }
    ?>
</div>
</body>
</html>
