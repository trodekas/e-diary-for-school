<?php
    require_once("../utils/studentSession.php");

    require_once("../libraries/User.php");
    $auth_user = new User();

    $user_id = $_SESSION['user_session'];

    $stmt = $auth_user->runQuery("SELECT * FROM users WHERE id=:user_id");
    $stmt->execute(array(":user_id"=>$user_id));

    $userRow=$stmt->fetch(PDO::FETCH_ASSOC);

    setlocale(LC_ALL, 'lt-LT');
    $currentMonth = date("m");
    $currentYear = date("Y");
    if(!isset($_GET['month'])) {
        $auth_user->redirect("./studentMark.php?month=". $currentMonth);
    }

    require_once("../libraries/Subject.php");
    $subject = new Subject();

    require_once("../libraries/Marks.php");
    $marks = new Marks();

    require_once("../libraries/Student.php");
    $student = new Student();

    require_once("../libraries/SemesterMark.php");
    $semesterMark = new SemesterMark();

    $currentStudent = $student->getStudent($user_id);

?>

<!DOCTYPE html>
<html>
<head>
    <title>Pažymiai</title>
    <link rel="import" href="../includes/basicHeadInclude.html">
</head>
<body>
<?php include_once '../includes/studentNavbar.php'; ?>

<div class="container">
    <div class="row">
        <ul class = "pagination">
            <li <?php if(isset($_GET['month']) && $_GET['month'] == 9) echo "class='active'"; ?>><a href = "./studentMark.php?month=09">Rugsėjis</a></li>
            <li <?php if(isset($_GET['month']) && $_GET['month'] == 10) echo "class='active'"; ?>><a href = "./studentMark.php?month=10">Spalis</a></li>
            <li <?php if(isset($_GET['month']) && $_GET['month'] == 11) echo "class='active'"; ?>><a href = "./studentMark.php?month=11">Lapkritis</a></li>
            <li <?php if(isset($_GET['month']) && $_GET['month'] == 12) echo "class='active'"; ?>><a href = "./studentMark.php?month=12">Gruodis</a></li>
            <li <?php if(isset($_GET['month']) && $_GET['month'] == 1) echo "class='active'"; ?>><a href = "./studentMark.php?month=01">Sausis</a></li>
            <li <?php if(isset($_GET['month']) && $_GET['month'] == 2) echo "class='active'"; ?>><a href = "./studentMark.php?month=02">Vasaris</a></li>
            <li <?php if(isset($_GET['month']) && $_GET['month'] == 3) echo "class='active'"; ?>><a href = "./studentMark.php?month=03">Kovas</a></li>
            <li <?php if(isset($_GET['month']) && $_GET['month'] == 4) echo "class='active'"; ?>><a href = "./studentMark.php?month=04">Balandis</a></li>
            <li <?php if(isset($_GET['month']) && $_GET['month'] == 5) echo "class='active'"; ?>><a href = "./studentMark.php?month=05">Gegužė</a></li>
        </ul>

        <?php
            if(isset($_GET['month']))
            {
                $daysInCurrentMonth = date("t");
                if($_GET['month'] >= 9 && $_GET['month'] <= 12 && $currentMonth  >= 9 && $currentMonth <= 12)
                {
                    $yearToSelect = date("Y");
                }
                else if($_GET['month'] >= 9 && $_GET['month'] <= 12 && $currentMonth  >= 1 && $currentMonth <= 5)
                {
                    $yearToSelect = date("Y") - 1;
                }
                else if($_GET['month'] >= 1 && $_GET['month'] <= 5 && $currentMonth  >= 9 && $currentMonth <= 12)
                {
                    $yearToSelect = date("Y") + 1;
                }
                else
                {
                    $yearToSelect = date("Y");
                }
                $allMarks = $marks->getAllMarksForYearMonthAndStudent($yearToSelect, $_GET['month'], $currentStudent['id']);
                if($allMarks->rowCount() > 0) {
                    $allMarks = $allMarks->fetchAll();
                }
                ?>
                <table class="table table-bordered">

                    <thead>
                    <tr>
                        <th rowspan="2">Dalykas</th>
                        <th colspan="<?php echo $daysInCurrentMonth?>">Diena</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php

                    ?><tr><?php
                        ?><td><?php echo ""; ?></td><?php
                    for($i = 1; $i <= $daysInCurrentMonth; $i++)
                    {
                        ?><td><?php echo $i; ?></td><?php
                    }
                    ?></tr><?php
                    $allSubjects = $subject->getAllSubjects();
                    if($allSubjects != null && $allSubjects->rowCount() > 0)
                    {
                        $allSubjects = $allSubjects->fetchAll();
                        $counter = 1;
                        foreach ($allSubjects as $s)
                        {
                            ?><tr><?php
                            ?><td><?php echo $s['name']; ?></td><?php
                            for($i = 1; $i <= $daysInCurrentMonth; $i++)
                            {
                                ?><td><?php
                                    foreach($allMarks as $m)
                                    {
                                        $timestamp = strtotime($m['date']);
                                        $mDay = date("d", $timestamp);
                                        if($i == $mDay && $counter == $m['fk_subjectid'])
                                        {
                                            echo $m['value'];
                                            break;
                                        }
                                        else
                                        {
                                            echo "";
                                        }
                                    }
                                ?></td><?php
                            }
                            ?></tr><?php

                            $counter++;
                        }
                    }
                    ?>
                    </tbody>

                </table>
                <?php
            }
        ?>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div>
                <h3>Rudens semestras:</h3>

                <table class="table table-hover">

                    <thead>
                    <tr>
                        <th>Dalykas</th>
                        <th>Vidurkis</th>
                        <th>Galutinis</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    $allSubjects = $subject->getAllSubjects();
                    if($allSubjects != null && $allSubjects->rowCount() > 0)
                    {
                        $allSubjects = $allSubjects->fetchAll();
                        $counter = 1;
                        foreach ($allSubjects as $s)
                        {
                            ?><tr><?php
                            ?><td><?php echo $s['name']; ?></td><?php
                            if($currentMonth >= 9 && $currentMonth <= 12)
                            {
                                $yearToSelect = date("Y");
                            }
                            else
                            {
                                $yearToSelect = date("Y") - 1;
                            }
                            $average = $marks->getAverageForStudentSubjectYearAndSemester($counter, $currentStudent['id'], $yearToSelect, 1);
                            ?><td><?php if($average['average'] == "") echo "-"; else echo $average['average']; ?></td><?php
                            $sM = $semesterMark->getSemesterMarkForStudentYearSubjectAndSemester($currentStudent['id'], $yearToSelect, $counter, 2);
                            ?><td><?php if(isset($sM['value'])) echo $sM['value']; else echo "-"; ?></td><?php
                            ?></tr><?php

                            $counter++;
                        }
                    }
                    ?>
                    </tbody>

                </table>
            </div>
        </div>
        <div class="col-md-6">
            <div>
                <h3>Pavasario semestras:</h3>

                <table class="table table-hover">

                    <thead>
                    <tr>
                        <th>Dalykas</th>
                        <th>Vidurkis</th>
                        <th>Galutinis</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                        $allSubjects = $subject->getAllSubjects();
                        if($allSubjects != null && $allSubjects->rowCount() > 0)
                        {
                            $allSubjects = $allSubjects->fetchAll();
                            $counter = 1;
                            foreach ($allSubjects as $s)
                            {
                                ?><tr><?php
                                ?><td><?php echo $s['name']; ?></td><?php
                                if($currentMonth >= 1 && $currentMonth <= 5)
                                {
                                    $yearToSelect = date("Y");
                                }
                                else
                                {
                                    $yearToSelect = date("Y") + 1;
                                }
                                $average = $marks->getAverageForStudentSubjectYearAndSemester($counter, $currentStudent['id'], $yearToSelect, 2);
                                ?><td><?php if($average['average'] == "") echo "-"; else echo $average['average']; ?></td><?php
                                $sM = $semesterMark->getSemesterMarkForStudentYearSubjectAndSemester($currentStudent['id'], $yearToSelect, $counter, 2);
                                ?><td><?php if(isset($sM['value'])) echo $sM['value']; else echo "-"; ?></td><?php
                                ?></tr><?php
                            }
                        }
                    ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>

</div>

</body>
</html>
