<?php
    require_once("../utils/studentSession.php");

    require_once("../libraries/User.php");
    $auth_user = new User();
    require_once("../libraries/Holiday.php");
    $holiday = new Holiday();

    $user_id = $_SESSION['user_session'];

    $stmt = $auth_user->runQuery("SELECT * FROM users WHERE id=:user_id");
    $stmt->execute(array(":user_id"=>$user_id));

    $userRow=$stmt->fetch(PDO::FETCH_ASSOC);

    $holidaysAutumnDates = $holiday->getHolidays(1);
    $holidaysSpringDates = $holiday->getHolidays(2);
?>

<!DOCTYPE html>
<html>
<head>
    <title>Atostogos</title>
    <link rel="import" href="../includes/basicHeadInclude.html">
</head>
<body>
<?php include_once '../includes/studentNavbar.php'; ?>

<div class="container">
    <div class="col-md-6">
        <div>
            <h2>Rudens atostogos:</h2>

            <table class="table table-hover">

                <thead>
                <tr>
                    <th>Pavadinimas</th>
                    <th>Nuo</th>
                    <th>Iki</th>
                </tr>
                </thead>

                <tbody>
                <?php
                    while($holidayDate = $holidaysAutumnDates->fetch(PDO::FETCH_ASSOC))
                    {
                        ?><tr><?php
                        ?><td><?php echo $holidayDate['name']; ?></td><?php
                        ?><td><?php echo $holidayDate['from']; ?></td><?php
                        ?><td><?php echo $holidayDate['to']; ?></td><?php
                        ?></tr><?php
                    }
                ?>
                </tbody>

            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div>
            <h2>Pavasario atostogos:</h2>

            <table class="table table-hover">

                <thead>
                <tr>
                    <th>Pavadinimas</th>
                    <th>Nuo</th>
                    <th>Iki</th>
                </tr>
                </thead>

                <tbody>
                <?php
                while($holidayDate = $holidaysSpringDates->fetch(PDO::FETCH_ASSOC))
                {
                    ?><tr><?php
                    ?><td><?php echo $holidayDate['name']; ?></td><?php
                    ?><td><?php echo $holidayDate['from']; ?></td><?php
                    ?><td><?php echo $holidayDate['to']; ?></td><?php
                    ?></tr><?php
                }
                ?>
                </tbody>

            </table>
        </div>
    </div>
</div>

</body>
</html>
