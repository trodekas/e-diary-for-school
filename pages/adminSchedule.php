<?php
require_once("../utils/adminSession.php");

require_once("../libraries/User.php");
$auth_user = new User();

$user_id = $_SESSION['user_session'];

$stmt = $auth_user->runQuery("SELECT * FROM users WHERE id=:user_id");
$stmt->execute(array(":user_id"=>$user_id));

$userRow=$stmt->fetch(PDO::FETCH_ASSOC);

require_once("../libraries/SchoolClass.php");
$schoolClass = new SchoolClass();

require_once("../libraries/Schedule.php");
$schedule = new Schedule();

require_once("../libraries/Subject.php");
$subject = new Subject();

require_once("../libraries/Teacher.php");
$teacher = new Teacher();

if(isset($_POST['submit']))
{
    if(!isset($_POST['schoolClass']))
    {
        $errorShow = "Nėra klasės";
    }
    else
    {
        $classForSchedule = $_POST['schoolClass'];
        $fullScheduleForClass = $schedule->getFullScheduleForClass($classForSchedule);
        if($fullScheduleForClass != null && $fullScheduleForClass->rowCount() > 0)
        {
            $fullScheduleForClass = $fullScheduleForClass->fetchAll();
        }
    }
}
else if(isset($_POST['submitDelete']))
{
    $schedule->deleteScheduleClass($_POST['deleteId']);
    $success = "Tvarkaraščio pamoka pašalinta";
}

if(isset($_GET['classId']) && isset($_GET['subjectId']))
{
    $classForSchedule = $_GET['classId'];
    $fullScheduleForClass = $schedule->getFullScheduleForClass($classForSchedule);
    if($fullScheduleForClass != null && $fullScheduleForClass->rowCount() > 0)
    {
        $fullScheduleForClass = $fullScheduleForClass->fetchAll();
    }
}

if(isset($_POST['submitNewScheduleClass']))
{
    if(!isset($_POST['subject']) || $_POST['subject'] == "")
    {
        $errorNew = "Nėra dalyko";
    }
    else if(!isset($_POST['classTeacher']))
    {
        $errorNew = "Nėra mokytojo";
    }
    else if(!isset($_POST['newScheduleDay']))
    {
        $errorNew = "Nėra dienos";
    }
    else if(!isset($_POST['newScheduleTime']))
    {
        $errorNew = "Nėra valandos";
    }
    else if($schedule->checkIfClassExistsForTeacherDayAndTime($_POST['classTeacher'], $_POST['newScheduleDay'], $_POST['newScheduleTime']))
    {
        $errorNew = "Tuo laiku mokytojas turi užsiėmimą";
    }
    else if($schedule->checkIfClassExistsForClassDayAndTime($_GET['classId'], $_POST['newScheduleDay'], $_POST['newScheduleTime']))
    {
        $errorNew = "Tuo laiku klasė turi užsiėmimą";
    }
    else
    {
        $newScheduleClass = $_GET['classId'];
        $newScheduleSubject = $_GET['subjectId'];
        $newScheduleTeacher = $_POST['classTeacher'];
        $newScheduleDay = $_POST['newScheduleDay'];
        $newScheduleTime = $_POST['newScheduleTime'];

        $schedule->addNewScheduleClass($newScheduleSubject, $newScheduleDay, $newScheduleTime, $newScheduleClass, $newScheduleTeacher);
        $successNew = "Nauja tvarkaraščio pamoka pridėta";

        $fullScheduleForClass = $schedule->getFullScheduleForClass($newScheduleClass);
        if($fullScheduleForClass != null && $fullScheduleForClass->rowCount() > 0)
        {
            $fullScheduleForClass = $fullScheduleForClass->fetchAll();
        }
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Tvarkaraštis</title>
    <link rel="import" href="../includes/basicHeadInclude.html">
</head>
<body>
<?php include_once '../includes/adminNavbar.php'; ?>

<div class="container">
    <h3>Tvarkaraštis</h3>

    <div class="row">
        <form method="post">
            <div class="form-group col-lg-3">
                <label for="schoolClass">Klasė:</label>
                <select class="form-control" id="schoolClass" name="schoolClass">
                    <?php
                    $allClasses = $schoolClass->getAllClasses();
                    if($allClasses != null && $allClasses->rowCount() > 0)
                    {
                        $allClasses = $allClasses->fetchAll();
                        foreach ($allClasses as $class)
                        {
                            ?><option value="<?php echo $class['classid']; ?>" <?php if(isset($_POST['schoolClass']) && $_POST['schoolClass'] == $class['classid']) echo "selected"; ?>><?php echo $class['classname']; ?></option><?php
                        }
                    }
                    else
                    {
                        ?><option value="" disabled selected>Nėra klasių</option><?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-lg-12">
                <input type="submit" class="btn btn-info" name="submit" value="Rodyti">
            </div>
        </form>
    </div>

    <?php
    if ( isset($error) )
    {

        ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $error ?>
        </div>
        <?php
    }
    else if ( isset($success) )
    {

        ?>
        <div class="alert alert-success" role="alert">
            <?php echo $success ?>
        </div>
        <?php
    }
    ?>

    <table class="table table-hover">

        <thead>
        <tr>
            <th>#</th>
            <th>Dalykas</th>
            <th>Mokytojas</th>
            <th>Kabinetas</th>
            <th>Diena</th>
            <th>Valanda</th>
            <th>Veiksmai</th>
        </tr>
        </thead>

        <tbody>
        <?php
        if(isset($fullScheduleForClass))
        {
            $counter = 1;
            foreach ($fullScheduleForClass as $fs)
            {
                ?><tr><?php
                ?><td><?php echo $counter; ?></td><?php
                ?><td><?php echo $fs['subjectname']; ?></td><?php
                ?><td><?php echo $fs['teachername'] . " " . $fs['teacherlastname']; ?></td><?php
                ?><td><?php echo $fs['roomname']; ?></td><?php
                ?><td><?php echo $fs['dayname']; ?></td><?php
                ?><td><?php echo $fs['timefrom'] . "-" . $fs['timeto']; ?></td><?php

                ?><td class="schedule-edit">
                <form method="post">
                    <input type="hidden" name="deleteId" value="<?php echo $fs['scheduleclassid']; ?>">
                    <input type="submit" class="btn btn-link" name="submitDelete" value="Trinti">
                    <a href="adminScheduleModification.php?modificationId=<?php echo $fs['scheduleclassid']?>" class="btn btn-link" role="button">Redaguoti</a>
                </form>
                </td><?php

                ?></tr><?php

                $counter++;
            }
        }
        ?>
        </tbody>
    </table>

    <?php
        if(isset($_POST['schoolClass']) || isset($_GET['subjectId']))
        {
            ?>
                <h3>Pridėti tvarkaraščio pamoką</h3>
                <div class="row">
                    <form method="post">
                        <div class="form-group col-lg-3">
                            <label for="subject">Dalykas:</label>
                            <select class="form-control" id="subject" name="subject" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                                <option value="">Pasirinkti...</option>
                                <?php
                                $allSubjects = $subject->getAllSubjects();
                                if($allSubjects != null && $allSubjects->rowCount() > 0)
                                {
                                    $allSubjects = $allSubjects->fetchAll();

                                    foreach ($allSubjects as $sub)
                                    {
                                        ?><option value="../pages/adminSchedule.php?subjectId=<?php echo $sub['id']; ?>&classId=<?php if(isset($_POST['schoolClass'])) echo $_POST['schoolClass']; else echo $_GET['classId']; ?>" <?php if(isset($_GET['subjectId']) && $_GET['subjectId'] == $sub['id']) echo " selected"; ?>><?php echo $sub['name']; ?></option><?php
                                    }
                                }
                                else
                                {
                                    ?><option value="" disabled selected>Nėra dalykų</option><?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="classTeacher">Mokytojas:</label>
                            <select class="form-control" id="classTeacher" name="classTeacher">
                                <?php
                                if(isset($_GET['subjectId']))
                                {
                                    if(!$schedule->checkIfClassHasScheduleClassForSubject($_GET['classId'], $_GET['subjectId']))
                                    {
                                        $allSubjectsTeachers = $teacher->getTeacherForSubject($_GET['subjectId']);
                                    }
                                    else
                                    {
                                        $allSubjectsTeachers = $teacher->getTeacherForSubjectAndClass($_GET['subjectId'], $_GET['classId']);
                                    }
                                    if ($allSubjectsTeachers != null && $allSubjectsTeachers->rowCount() > 0)
                                    {
                                        $allSubjectsTeachers = $allSubjectsTeachers->fetchAll();

                                        foreach ($allSubjectsTeachers as $sub)
                                        {
                                            ?><option value="<?php echo $sub['id']; ?>"><?php echo $sub['name'] . " " . $sub['lastname']; ?></option><?php
                                        }
                                    }
                                    else
                                    {
                                        ?><option value="" disabled selected>Nėra mokytojų</option><?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="newScheduleDay">Diena:</label>
                            <select class="form-control" id="newScheduleDay" name="newScheduleDay">
                                <?php
                                    $allDays = $schedule->getAllScheduleDays();

                                    if ($allDays != null && $allDays->rowCount() > 0)
                                    {
                                        $allDays = $allDays->fetchAll();

                                        foreach ($allDays as $sub)
                                        {
                                            ?><option value="<?php echo $sub['id']; ?>"><?php echo $sub['name']; ?></option><?php
                                        }
                                    }
                                    else
                                    {
                                        ?><option value="" disabled selected>Nėra dienų</option><?php
                                    }

                                ?>
                            </select>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="newScheduleTime">Laikas:</label>
                            <select class="form-control" id="newScheduleTime" name="newScheduleTime">
                                <?php
                                $allTimes = $schedule->getAllScheduleTimes();

                                if ($allTimes != null && $allTimes->rowCount() > 0)
                                {
                                    $allTimes = $allTimes->fetchAll();

                                    foreach ($allTimes as $sub)
                                    {
                                        ?><option value="<?php echo $sub['id']; ?>"><?php echo $sub['fromTime'] . "-" . $sub['toTime']; ?></option><?php
                                    }
                                }
                                else
                                {
                                    ?><option value="" disabled selected>Nėra laikų</option><?php
                                }

                                ?>
                            </select>
                        </div>
                        <div class="form-group col-lg-12">
                            <input type="submit" class="btn btn-info" name="submitNewScheduleClass" value="Pridėti">
                        </div>
                    </form>
                </div>
            <?php
        }
    ?>

    <?php
    if ( isset($errorNew) )
    {
        ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $errorNew ?>
        </div>
        <?php
    }
    else if ( isset($successNew) )
    {
        ?>
        <div class="alert alert-success" role="alert">
            <?php echo $successNew ?>
        </div>
        <?php
    }
    ?>

</div>
</body>
</html>
