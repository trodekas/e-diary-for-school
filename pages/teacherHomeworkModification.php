<?php
require_once("../utils/teacherSession.php");

require_once("../libraries/User.php");
$auth_user = new User();
require_once ("../libraries/Report.php");
$report = new Report();

$user_id = $_SESSION['user_session'];

$stmt = $auth_user->runQuery("SELECT * FROM users WHERE id=:user_id");
$stmt->execute(array(":user_id"=>$user_id));

$userRow=$stmt->fetch(PDO::FETCH_ASSOC);

require_once("../libraries/Teacher.php");
$teacher = new Teacher();
$teacherInfo = $teacher->getTeacher($user_id);

require_once("../libraries/Homework.php");
$homework = new Homework();
$currentHomework = $homework->getHomework($_GET['modificationId']);

require_once("../libraries/Schedule.php");
$schedule = new Schedule();
$teachersClasses = $schedule->getClassListForTeacher($teacherInfo['id']);
if($teachersClasses != null)
{
    $teachersClasses = $teachersClasses->fetchAll();
}

if(isset($_POST['submit']))
{
    if(!isset($_POST['schoolClass']))
    {
        $errorNew = "Nėra klasės";
    }
    else {
        $homeworkSummary = $_POST['summary'];
        $homeworkClass = $_POST['schoolClass'];
        $homeworkDate = $_POST['toDate'];

        if (strlen($homeworkSummary) < 1) {
            $errorNew = "Aprašymas turi būti užpildytas";
        } else if (strlen($homeworkDate) < 1) {
            $errorNew = "Data turi būti užpildyta";
        } else {
            $homework->updateHomework($homeworkClass, $homeworkDate, $homeworkSummary, $_GET['modificationId']);
            $successNew = "Namų darbas sėkmingai redaguotas";

            $currentHomework = $homework->getHomework($_GET['modificationId']);
        }
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Namų darbo modifikavimas</title>
    <link rel="import" href="../includes/basicHeadInclude.html">
</head>
<body>
<?php include_once '../includes/teacherNavbar.php'; ?>

<div class="container">
    <h3>Redaguoti namų darbą</h3>

    <div class="row">
        <form method="post">
            <div class="form-group col-lg-3">
                <label for="schoolClass">Klasė:</label>
                <select class="form-control" id="schoolClass" name="schoolClass">
                    <?php
                    foreach ($teachersClasses as $class)
                    {
                        ?><option <?php if($class['fk_classid'] == $currentHomework['fk_classid']) echo "selected"; ?> value="<?php echo $class['fk_classid']; ?>"><?php echo $class['name']; ?></option><?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-lg-3">
                <label for="toDate">Iki:</label>
                <input type="date" class="form-control" id="toDate" name="toDate" min="<?php echo date("Y-m-d"); ?>" value="<?php echo $currentHomework['end']?>">
            </div>
            <div class="form-group col-lg-6">
                <label for="summary">Aprašas:</label>
                <input type="text" class="form-control" id="summary" name="summary" value="<?php echo $currentHomework['summary']?>">
            </div>
            <div class="form-group col-lg-4">
                <input type="submit" class="btn btn-info" name="submit" value="Išsaugoti">
            </div>

        </form>
    </div>

    <?php
    if ( isset($errorNew) )
    {

        ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $errorNew ?>
        </div>
        <?php
    }
    else if ( isset($successNew) )
    {

        ?>
        <div class="alert alert-success" role="alert">
            <?php echo $successNew ?>
        </div>
        <?php
    }
    ?>

</div>
</body>
</html>
