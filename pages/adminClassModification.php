<?php
require_once("../utils/adminSession.php");

require_once("../libraries/User.php");
$auth_user = new User();

$user_id = $_SESSION['user_session'];

$stmt = $auth_user->runQuery("SELECT * FROM users WHERE id=:user_id");
$stmt->execute(array(":user_id"=>$user_id));

$userRow=$stmt->fetch(PDO::FETCH_ASSOC);

require_once("../libraries/Teacher.php");
$teacher = new Teacher();

require_once("../libraries/SchoolClass.php");
$schoolClass = new SchoolClass();

$currentSchoolClass = $schoolClass->getClass($_GET['modificationId']);

require_once("../libraries/Student.php");
$student = new Student();

if(isset($_POST['submitNewTeacher']))
{
    $classNewTeacher = $_POST['schoolClassLeader'];
    if($classNewTeacher == "")
    {
        $errorNew = "Neapsirinktas mokytojas";
    }
    else
    {
        $schoolClass->updateClassTeacher($currentSchoolClass['classid'], $classNewTeacher);

        $successNew = "Mokytojas atnaujintas";
        $currentSchoolClass = $schoolClass->getClass($_GET['modificationId']);
    }
}
else if(isset($_POST['submitNewStudent']))
{
    $classNewStudent = $_POST['schoolClassStudent'];
    if($classNewStudent == "")
    {
        $errorNew = "Moksleivis nepasirinktas";
    }
    else
    {
        $student->updateStudentClass($classNewStudent, $currentSchoolClass['classid']);

        $successNew = "Moksleivis pridėtas";
        $currentSchoolClass = $schoolClass->getClass($_GET['modificationId']);
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Klasės modifikavimas</title>
    <link rel="import" href="../includes/basicHeadInclude.html">
</head>
<body>
<?php include_once '../includes/adminNavbar.php'; ?>

<div class="container">
    <h3>Redaguoti klasę</h3>
    <div class="row">
        <form method="post">
            <div class="form-group col-lg-4">
                <label for="schoolClassLeader">Vadovas:</label>
                <select class="form-control" id="schoolClassLeader" name="schoolClassLeader">
                    <?php
                    $teachersWithoutClass = $teacher->getAllTeachersWithoutClass();
                    if($teachersWithoutClass != null && $teachersWithoutClass->rowCount() > 0) {
                        $teachersWithoutClass = $teachersWithoutClass->fetchAll();
                        foreach ($teachersWithoutClass as $t) {
                            ?><option value="<?php echo $t['teacherid']; ?>"><?php echo $t['name'] . " " . $t['lastname']; ?></option><?php
                        }
                    }

                    ?><option selected value="<?php echo $currentSchoolClass['teacherid']; ?>"><?php echo $currentSchoolClass['teachername'] . " " . $currentSchoolClass['teacherlastname']; ?></option><?php
                    ?>
                </select>
            </div>
            <div class="form-group col-lg-12">
                <input type="submit" class="btn btn-info" name="submitNewTeacher" value="Išsaugoti">
            </div>

        </form>
    </div>
    <div class="row">
        <form method="post">
            <div class="form-group col-lg-4">
                <label for="schoolClassStudent">Naujas mokinys:</label>
                <select class="form-control" id="schoolClassStudent" name="schoolClassStudent">
                    <?php
                    $allStudents = $student->getAllStudentNotInClass($currentSchoolClass['classid']);
                    if($allStudents != null && $allStudents->rowCount() > 0) {
                        $allStudents = $allStudents->fetchAll();
                        foreach ($allStudents as $s) {
                            ?><option value="<?php echo $s['id']; ?>"><?php echo $s['name'] . " " . $s['lastname']; ?></option><?php
                        }
                    }
                    else
                    {
                        ?><option value="" disabled selected>Nėra moksleivių</option><?php
                    }

                    ?>
                </select>
            </div>
            <div class="form-group col-lg-12">
                <input type="submit" class="btn btn-info" name="submitNewStudent" value="Pridėti">
            </div>

        </form>
    </div>
    <?php
    if ( isset($errorNew) )
    {

        ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $errorNew ?>
        </div>
        <?php
    }
    else if ( isset($successNew) )
    {

        ?>
        <div class="alert alert-success" role="alert">
            <?php echo $successNew ?>
        </div>
        <?php
    }
    ?>

    <h3>Mokinių sąrašas</h3>

    <table class="table table-hover">

        <thead>
        <tr>
            <th>#</th>
            <th>Vardas</th>
            <th>Pavardė</th>
            <th>Klasė</th>
        </tr>
        </thead>

        <tbody>
        <?php
        $studentList = $student->getAllStudentsForClass($currentSchoolClass['classid']);
        if($studentList != null && $studentList->rowCount() > 0)
        {
            $studentList = $studentList->fetchAll();
            $counter = 1;
            foreach ($studentList as $s)
            {
                ?><tr><?php
                ?><td><?php echo $counter; ?></td><?php
                ?><td><?php echo $s['name']; ?></td><?php
                ?><td><?php echo $s['lastname']; ?></td><?php
                ?><td><?php echo $currentSchoolClass['classname']; ?></td><?php
                ?></tr><?php

                $counter++;
            }
        }
        ?>
        </tbody>
    </table>

</div>
</body>
</html>
