<?php
    require_once("../utils/teacherSession.php");

    require_once("../libraries/User.php");
    $auth_user = new User();

    $user_id = $_SESSION['user_session'];

    $stmt = $auth_user->runQuery("SELECT * FROM users WHERE id=:user_id");
    $stmt->execute(array(":user_id"=>$user_id));

    $userRow=$stmt->fetch(PDO::FETCH_ASSOC);

    require_once("../libraries/Teacher.php");
    $teacher = new Teacher();

    $teacherInfo = $teacher->getTeacher($user_id);

    require_once ("../libraries/Schedule.php");
    $schedule = new Schedule();

    $allScheduleTime = $schedule->getAllScheduleTimes();
    $scheduleDays = $schedule->getAllScheduleDays();
    $scheduleDays = $scheduleDays->fetchAll();
?>

<!DOCTYPE html>
<html>
<head>
    <title>Tvarkaraštis</title>
    <link rel="import" href="../includes/basicHeadInclude.html">
</head>
<body>
<?php include_once '../includes/teacherNavbar.php'; ?>

<div class="container">
    <div>
        <h2>Tvarkaraštis</h2>

        <table class="table table-hover table-bordered">

            <thead>
            <tr>
                <th>Laikas</th>
                <?php
                foreach ($scheduleDays as $scheduleDay)
                {
                    ?><th><?php echo $scheduleDay['name']; ?></th><?php
                }
                ?>
            </tr>
            </thead>

            <tbody>
            <?php

            while($scheduleTime = $allScheduleTime->fetch(PDO::FETCH_ASSOC))
            {
                ?><tr><?php
                ?><td><?php echo $scheduleTime['fromTime'] ."-". $scheduleTime['toTime']; ?></td><?php
                foreach ($scheduleDays as $scheduleDay)
                {
                    ?><td><?php $subject = $schedule->getSubjectForTeacherTimeAndDay($teacherInfo['id'], $scheduleTime['id'], $scheduleDay['id']); if($subject['classname'] != "") echo $subject['classname']; else echo "-"; ?></td><?php
                }
                ?></tr><?php
            }
            ?>
            </tbody>

        </table>
    </div>
</div>

</body>
</html>