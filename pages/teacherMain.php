<?php
    require_once("../utils/teacherSession.php");

    require_once("../libraries/User.php");
    $auth_user = new User();
    require_once ("../libraries/Report.php");
    $report = new Report();

    $user_id = $_SESSION['user_session'];

    $stmt = $auth_user->runQuery("SELECT * FROM users WHERE id=:user_id");
    $stmt->execute(array(":user_id"=>$user_id));

    $userRow=$stmt->fetch(PDO::FETCH_ASSOC);

    require_once ("../libraries/Schedule.php");
    $schedule = new Schedule();
    $scheduleTimes = $schedule->getAllScheduleTimes();
    $scheduleTimes = $scheduleTimes->fetchAll();

    require_once("../libraries/Teacher.php");
    $teacher = new Teacher();
    $teacherInfo = $teacher->getTeacher($user_id);
?>

<!DOCTYPE html>
<html>
<head>
    <title>Pradžia</title>
    <link rel="import" href="../includes/basicHeadInclude.html">
</head>
<body>
<?php include_once '../includes/teacherNavbar.php'; ?>

<div class="container">
    <div class="col-md-6">
        <div>
            <h2>Šiandienos tvarkaraštis:</h2>

            <table class="table table-hover">

                <thead>
                <tr>
                    <th colspan="3"><?php setlocale(LC_ALL, 'lt-LT'); $currentDay = strftime("%A", time()); echo ucfirst($currentDay);?></th>
                </tr>
                </thead>

                <tbody>
                <tr>
                <?php
                foreach($scheduleTimes as $scheduleTime)
                {
                    ?><tr><?php
                    ?><td><?php echo $scheduleTime['fromTime'] ."-". $scheduleTime['toTime']; ?></td><?php
                    $subject = $schedule->getTodaysSubjectForTeacherAndTime($teacherInfo['id'], $scheduleTime['id']);
                    ?><td><?php if($subject['subjectname'] != "") echo $subject['subjectname']; else echo "-"; ?></td><?php
                    ?><td><?php if($subject['classname'] != "") echo $subject['classname']; else echo "-"; ?></td><?php
                    ?></tr><?php
                }
                ?>
                </tr>
                </tbody>

            </table>
        </div>
    </div>
    <div class="col-md-6">
        <h2>Dienos pranešimas:</h2>

        <textarea class="form-control" rows="15" id="report" style="resize: none" readonly><?php echo $report->getReportOfTheDay();?></textarea>
    </div>
</div>
</body>
</html>