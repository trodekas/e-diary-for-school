<?php
    require_once("../utils/adminSession.php");

    require_once("../libraries/User.php");
    $auth_user = new User();
    require_once ("../libraries/Report.php");
    $report = new Report();

    $user_id = $_SESSION['user_session'];

    $stmt = $auth_user->runQuery("SELECT * FROM users WHERE id=:user_id");
    $stmt->execute(array(":user_id"=>$user_id));

    $userRow=$stmt->fetch(PDO::FETCH_ASSOC);

    if(isset($_POST['submit']))
    {
        $report->updateReportOfTheDay($_POST['report']);

        $success = "Pranešimas atnaujintas";
    }
?>

<!DOCTYPE html>
<html>
<head>
    <title>Pradžia</title>
    <link rel="import" href="../includes/basicHeadInclude.html">
</head>
<body>
<?php include_once '../includes/adminNavbar.php'; ?>

<div class="container">
    <div>
        <h2>Dienos pranešimas:</h2>
    </div>

    <form method="post">
        <div class="form-group">
            <textarea class="form-control" rows="15" id="report" name="report" style="resize: none"><?php echo $report->getReportOfTheDay();?></textarea>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-info" name="submit" value="Išsaugoti">
        </div>

        <?php
        if ( isset($success) )
        {

            ?>
            <div class="alert alert-success" role="alert">
                <?php echo $success ?>
            </div>
            <?php
        }
        ?>
    </form>
</div>

</body>
</html>