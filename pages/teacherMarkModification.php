<?php
require_once("../utils/teacherSession.php");

require_once("../libraries/User.php");
$auth_user = new User();
require_once ("../libraries/Report.php");
$report = new Report();

$user_id = $_SESSION['user_session'];

$stmt = $auth_user->runQuery("SELECT * FROM users WHERE id=:user_id");
$stmt->execute(array(":user_id"=>$user_id));

$userRow=$stmt->fetch(PDO::FETCH_ASSOC);

require_once("../libraries/Teacher.php");
$teacher = new Teacher();
$teacherInfo = $teacher->getTeacher($user_id);

require_once("../libraries/Marks.php");
$marks = new Marks();
$currentMark = $marks->getMark($_GET['modificationId']);

if(isset($_POST['submit']))
{
    $newMark = $_POST['mark'];
    $markSummary = $_POST['summary'];
    $markDate = $_POST['markDate'];
    $markId = $_GET['modificationId'];

    if (strlen($markDate) < 1) {
        $errorNew = "Data turi būti nurodyta";
    }
    else if(strlen($newMark) < 1)
    {
        $errorNew = "Pažymys turi būti nurodytas";
    }
    else
    {
        $marks->updateMark($newMark, $markSummary, $markDate, $markId);
        $successNew = "Pažymys atnaujintas";
        $currentMark = $marks->getMark($_GET['modificationId']);
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Pažymio modifikavimas</title>
    <link rel="import" href="../includes/basicHeadInclude.html">
</head>
<body>
<?php include_once '../includes/teacherNavbar.php'; ?>

<div class="container">
    <h3>Redaguoti pažymį</h3>

    <div class="row">
        <form method="post">
            <div class="form-group col-lg-4">
                <label for="mark">Pažymis:</label>
                <input type="text" class="form-control" id="mark" name="mark" pattern="^[1-9]|10$" oninvalid="this.setCustomValidity('Neteisingai užpildytas laukas')" oninput="setCustomValidity('')" value="<?php echo $currentMark['value']?>">
            </div>
            <div class="form-group col-lg-4">
                <label for="summary">Aprašas:</label>
                <input type="text" class="form-control" id="summary" name="summary" value="<?php echo $currentMark['summary']?>">
            </div>
            <div class="form-group col-lg-4">
                <label for="markDate">Iki:</label>
                <input type="date" class="form-control" id="markDate" name="markDate" value="<?php echo $currentMark['date']?>">
            </div>
            <div class="form-group col-lg-4">
                <input type="submit" class="btn btn-info" name="submit" value="Įvesti">
            </div>
        </form>
    </div>

    <?php
    if ( isset($errorNew) )
    {

        ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $errorNew ?>
        </div>
        <?php
    }
    else if ( isset($successNew) )
    {

        ?>
        <div class="alert alert-success" role="alert">
            <?php echo $successNew ?>
        </div>
        <?php
    }
    ?>

</div>
</body>
</html>
