### What is this repository for? ###

Simple school's e-diary website created using PHP, HTML, SQL and Bootstrap for university assignment

Use cases:

- Students can check their homeworks, grades, schedule and etc.

- Teachers can assign homeworks to classes, grade students and etc.