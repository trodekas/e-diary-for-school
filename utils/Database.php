<?php

require_once ('config.php');

class Database
{
    public $connection;

    public function connect()
    {
        $this->connection = null;
        try
        {
            $this->connection = new PDO("mysql:host=" . config::DB_SERVER . ";port=3306;dbname=" . config::DB_NAME, config::DB_USERNAME, config::DB_PASSWORD);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $exception)
        {
            echo "Connection error: " . $exception->getMessage();
        }

        return $this->connection;
    }
}