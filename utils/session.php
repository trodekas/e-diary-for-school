<?php

session_start();

require_once '../libraries/User.php';
$session = new USER();

if(!$session->isLoggedIn())
{
    $session->redirect('../index.php');
}