<?php

class Patterns
{
    const ROOM_NAME = '/^\d{3}[a-z]{0,1}$/';
    const CITIZEN_CODE = '/^\d{11}$/';
    const ONLY_LETTERS = '/^[a-zą-žA-ZĄ-Ž]+$/';
    const ONLY_NUMBERS = '/^[0-9]+$/';
}