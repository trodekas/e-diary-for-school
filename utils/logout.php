<?php
require_once('session.php');
require_once('../libraries/User.php');
$user_logout = new USER();

/*
if($user_logout->isLoggedIn()!="")
{
    $user_logout->redirect('../pages/studentMain.php');
}
*/
if(isset($_GET['logout']) && $_GET['logout']=="true")
{
    $user_logout->doLogout();
    $user_logout->redirect('../index.php');
}
