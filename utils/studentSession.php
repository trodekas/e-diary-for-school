<?php
header('Content-Type: text/html; charset=utf-8');
session_start();

require_once '../libraries/User.php';
$session = new USER();

if(!$session->isLoggedIn())
{
    $session->redirect('../index.php');
}
else
{
    $userLevel = $session->getUserLevel($_SESSION['user_session']);

    if($userLevel == config::TEACHER_LEVEL)
    {
        $session->redirect('teacherMain.php');
    }
    else if($userLevel == config::ADMIN_LEVEL)
    {
        $session->redirect('adminMain.php');
    }
}